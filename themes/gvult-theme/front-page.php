<?php 
	get_header();

	?>
	<div id="front-page-slider">
	<?php
	include 'php/slider-main.php';
	?>
	</div>


<div class="container-fluid">
	<div class="row contentDiv1 blockMargin">
		<div  class="col-sm-8 col-sm-12">

			<div class="row firstDivContainer">
				<?php
				$args = array
				(
					'posts_per_page'	=> 1,
					'offset'			=> 6,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();
				 	$date = get_the_date('d F Y H:i', $post);
					$category = get_the_category();
					$categoryName = $category[0]->category_parent 
									? get_cat_name($category[0]->category_parent) 
									: $category[0]->name;?>
					<a href="<?php echo get_the_permalink();?>" class="col-xs-12 blackoutPictureLink typeBlock1" title="<?php the_title();?>">
						<figure class="mainpage-card-triple">
							<div class="blockPictureDate">
								<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive" alt="<?php the_title(); ?>">
								<div class="shadow_card"><p class="blockHiddenDate"><?php echo $date; ?></p></div>
							</div>
							<figcaption>
								<h5 class="cardHearedH5"><?php echo $categoryName;?></h5>
								<p class="paragraf1"><?php the_title();?></p>

							</figcaption>
						</figure>
					</a>
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div>

			<?php

				$args = array
				(
					'posts_per_page'	=> 1,
					'offset'			=> 7,
					'post_type'			=> array('post', 'places')
				);
  

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();
					$date = get_the_date('d F Y H:i', $post);
					$category = get_the_category();
					$categoryName = $category[0]->category_parent 
									? get_cat_name($category[0]->category_parent) 
									: $category[0]->name;
					?>
				<a href="<?php echo get_the_permalink();?>" class="col-xs-12 col-sm-8  blackoutPictureLink typeBlock3 col-sm-push-4"  title="">
					<figure class="main-card-double ">
							<div class="blockPictureDate">
								<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
								<div class="shadow_card"><p class="blockHiddenDate"><?php echo $date; ?></p></div>
							</div>
							<figcaption>
								<h5 class="cardHearedH5"><?php echo $categoryName;?></h5>
								<p class="paragraf"><?php the_title();?></p>
							</figcaption>
					</figure>
				</a>
				
				<?php
				endwhile;
				wp_reset_postdata();

				$args = array
				(
					'posts_per_page'	=> 1,
					'post_type'			=> array('review')
				);

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();?>
					<a href="<?php echo get_the_permalink();?>" class="col-xs-6 col-sm-4 padding_zero blackoutPictureLink typeBlock2 col-sm-pull-8"  title="">
						<figure class="blueBorderBlock main-review-card-small">
							<figcaption>
								<p class="border-card-par"><?php the_title();?></p>
							</figcaption>
							<div class="blockPictureDate">
								<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
							</div>
						</figure>
					</a>
					<?php
				endwhile;
				wp_reset_postdata();
					
				

				$args = array
				(
					'posts_per_page'	=> 1,
					'offset'			=> 8,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();
					$date = get_the_date('d F Y H:i', $post);
					$category = get_the_category();
					$categoryName = $category[0]->category_parent 
									? get_cat_name($category[0]->category_parent) 
									: $category[0]->name;
					?>
			
			
				<a href="<?php echo get_the_permalink();?>"  class="col-xs-6 col-sm-4 padding_zero blackoutPictureLink typeBlock4" title="">
					<figure class=" main-card-single">
							<div class="blockPictureDate">
								<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
								<div class="shadow_card"><p class="blockHiddenDate"><?php echo $date; ?></p></div>
							</div>
							<figcaption>
								<h5 class="cardHearedH5"><?php echo $categoryName;?></h5>
								<p class="paragraf"><?php the_title();?></p>
							</figcaption>
					</figure>
				</a>
				<?php
				endwhile;
				wp_reset_postdata();

				$args = array
				(
					'posts_per_page'	=> 1,
					'offset'			=> 9,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();
					$date = get_the_date('d F Y H:i', $post);
					$category = get_the_category();
					$categoryName = $category[0]->category_parent 
									? get_cat_name($category[0]->category_parent) 
									: $category[0]->name;
					?>
					
					<a href="<?php echo get_the_permalink();?>" class="col-xs-6 col-sm-4 padding_zero blackoutPictureLink typeBlock4 col-sm-push-4"  title="">
						<figure class="main-card-single ">
								<div class="blockPictureDate">
									<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
									<div class="shadow_card"><p class="blockHiddenDate"><?php echo $date; ?></p></div>
								</div>
								<figcaption>
									<h5 class="cardHearedH5"><?php echo $categoryName;?></h5>
									<p class="paragraf"><?php the_title();?></p>
								</figcaption>
						</figure>
					</a>
				<?php
				endwhile;
				wp_reset_postdata();

				$args = array
				(
					'posts_per_page'	=> 1,
					'offset'			=> 1,
					'post_type'			=> array('review')
				);

				$query = new WP_Query($args);

				while ( $query->have_posts() ) :
					$query->the_post();?>
					<a href="<?php echo get_the_permalink();?>" class="col-xs-6 col-sm-4 blackoutPictureLink typeBlock2 col-sm-pull-4 front_page_margin_card"  title="">
						<figure class="blueBorderBlock  main-review-card-small">
								<figcaption>
									<!-- <p class="border-card-num">8</p> -->
									<p class="border-card-par"><?php the_title();?></p>
								</figcaption>
								<div class="blockPictureDate">
									<img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
								</div>
						</figure>
					</a>
				<?php
				endwhile;
				wp_reset_postdata();

				?>
			<div class="row firstDivContainer  padding_zero">
				
				<div class="col-sm-6 col-xs-12 main_phone_adwertis">
					<a href="#"  title="">
						<div class="blockPictureDate">
							<img src="http://placehold.it/300x250" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
						</div>
					</a>
				</div>
				<div class="col-sm-6  col-xs-12 blackoutPictureLink  main_phone_adwertis">
					<a href="#"  title="">
						<div class="blockPictureDate">
							<img src="http://placehold.it/300x250" class="img-responsive categoryImages" alt="<?php the_title(); ?>">
						</div>
					</a>
				</div>
			</div>	

		</div>
		<div class="col-sm-4 col-xs-4  lastNewsblock">
			<div class=" firstDivContainer  margin_zero padding_zero blockPictureDate">
				<a href="#" class="blackoutPictureLink"  title="">
					<div class="blockPictureDate">
						<img src="http://placehold.it/300x600" class="img-responsive" alt="<?php the_title(); ?>">
					</div>
				</a>
			</div>

			<div class="firstDivContainer  ">
			<?php
				include 'php/slider-news.php';
			?>
			</div>
		</div>
	</div>
	<div class="row contentDiv3">
		<div class="col-xs-12 catalog_block">	
			<div class="col-md-12 catalogMainHead">
				<p class="bigHeaderParagraf-inx1">city guide</p>
			</div>
			<?php
				include 'php/card-places.php';
			?>
		</div>
	</div>
	<div class="row contentDiv4">
		<div class="col-sm-4 col-xs-12 col-sm-push-8">
			<div class="reviews">
				<h4 class="last-reviews-head">Последние отзывы заведений</h4>
					<?php echo ss_getComments(3, 0, 3, 'reviewBlocks');?>
				<div>
					<!-- <a href="#" class="btn-standart-border ind-review-btn"  title="">БОЛЬШЕ ОТЗЫВОВ</a> -->
				</div>
			</div>
		</div>
		<?php
			$args = array(
				'posts_per_page'	=> 1,
				'category__not_in'	=> 'city-guide',
				'offset'			=> 12,
				'post_type'			=> 'post',
				'order'			=> 'ASC'
			);

			$gvPost = new WP_Query($args);
			if($gvPost->have_posts()) :

				while($gvPost->have_posts()) : $gvPost->the_post();?>
				<div class="col-sm-8 col-xs-12 col-sm-pull-4">
					<a href="#"  title="">
						<figure class="blueBorder">
							<div class="blueBorder-blockImg">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive blueBorderImg" alt="<?php the_title(); ?>">
								</div>
							<figcaption class="mainReview">
								<h5 class="cardHearedH5"><?php the_category($post->ID); ?></h5>
								<p class="paragraf"><?php the_title(); ?></p>
							</figcaption>
						</figure>
					</a>
				</div>
				<?php
				endwhile;
			endif;
			wp_reset_postdata();
			?>
	</div>
	<div  class="row contentDiv5">
		<div class="row margin_zero author-posts-cont">
		<?php

			$args = array(
				'posts_per_page'	=>1,
				'post_type' 		=>'review',
				'offset'			=> 5
			);

			$gvPost = new WP_Query($args);
			if($gvPost->have_posts()) {
				while($gvPost->have_posts()){
					$gvPost->the_post();

		?>
				<div class="col-sm-4 col-xs-12 div5Block2 author-block-small col-md-push-8">
					<a href="#" class="author-blockSM-pars"  title="">
						<p class="author-block-title"><?php the_title(); ?></p>
						<p class="author-block-name"><?php the_author(); ?></p>
					</a>
				</div>

		<?php

					}
				}
				wp_reset_postdata();
		?>
		<?php

			$args = array(
				'posts_per_page'	=>1,
				'post_type' 		=>'review',
			);

			$gvPost = new WP_Query($args);
			if($gvPost->have_posts()) {
				while($gvPost->have_posts()){
					$gvPost->the_post();

		?>
			
				<div class="col-sm-8 col-xs-12 div5Block1 author-block-large col-md-pull-4">
					<a href="#" class="author-blockLR-pars"  title="">
						<p class="author-block-title"><?php the_title(); ?></p>
						<p class="author-block-name"><?php the_author(); ?></p>
					</a>
				</div>
				
		<?php

					}
				}
				wp_reset_postdata();
		?>
		
		</div>
	</div>
	<div class="row margin_zero padding_zero">
		<div class="row">
		<div class="col-xs-12 bigHeader">
			<p class="bigHeaderParagraf-inx2">премьеры</p>
		</div>
		</div>	
  	</div>
  	<div class="row ind-secondslider">
	  <?php
	  	include 'php/slider-events.php';
	  ?>
	</div>
	<div class="row contentDiv6">
		<div class="col-xs-12 contentDiv6_block">
           <?php
				$args = array(
					'posts_per_page'	=> 1,
					'post_type'			=> 'review'
				);
				$gvPost = new WP_Query($args);
				if($gvPost->have_posts()) :
					while($gvPost->have_posts()) : 
						$gvPost->the_post();
						?>
						<div style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);" class="col-xs-12 premiereRevBanner">
							<div class="col-sm-7 premiereRevBanner-cont">
								<p class="bigHeaderParagraf-inx3">обзор</p>
								<a href="#"  title="">
									<p class="premiereRevH2"><?php the_title(); ?></p>
									<p class="premiereRevParagraf">
										<?php echo strip_tags(wp_trim_words( get_the_content(), 20 )); ?>
									</p>
								</a>
							</div>
						</div>
					<?php
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>
	</div>
	<div class="row">
			<div class="col-sm-12">
			<?php
			$args = array
				(
					'posts_per_page'	=> 8,
					'offset'			=> 20,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);
				$count = 0;
				
				while ( $query->have_posts() )
				{
					$query->the_post();

					if ( $count == 2 || $count == 4 || $count == 5 || $count == 6 || $count == 7){
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink phone_card_delete typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}
					
					elseif ( $count != 3 )
					{
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}
					else
					{
						echo '<div class="col-sm-8 blackoutPictureLink front_border_card">';
						echo getCardSimpleDoubleWithBorder($post);
						echo '</div>';
					}

					$count++;
				}
				
				wp_reset_postdata();?>
			</div>
		</div>

	<div class="row margin_zero padding_zero">
		<div class="row">
			<div class="col-xs-12 bigHeader">
				<p class="bigHeaderParagraf-inx2">follow us</p>
			</div>
		</div>	
	</div>
	<div class="row socialBanner  margin_zero padding_zero">
		<div class="social_phone_background">
		</div>
		<div class="col-xs-12 socialBannerIcons">
			<p class="catalogHeaders">Подпишитесь на нас в социальных сетях</p>
			<div>
				<a href="https://www.facebook.com/GVULT/"  title=""><img src="<?php bloginfo('template_url'); ?>/img/facebook.png" alt="facebook icon"></a>
				<a href="https://www.instagram.com/gvult/" class="instagramIcon"  title=""><img src="<?php bloginfo('template_url'); ?>/img/instagram.png" alt="instagram icon"></a>
				<a href="http://gvult.com/feed"  title=""><img src="<?php bloginfo('template_url'); ?>/img/r-s-s.png" alt="rss icon"></a>
			</div>	
		</div>
	</div>

	<div id="container_posts" class="row contentDiv7">
		<?php
			$args = array
				(
					'posts_per_page'	=> 5,
					'offset'					=> 50,
					'post_type'				=> array('post', 'places')
				);

			$query = new WP_Query($args);
			
			$count = 0;
			while ( $query->have_posts() )
			{
				$query->the_post();

				if ( $count == 2 || $count == 4 || $count == 5 || $count == 6 || $count == 7){
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink phone_card_delete typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}

									
				elseif ( $count != 3 )
				{
					echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335">';
					echo getCardSimpleSingle($post);
					echo '</div>';
				}
				else
				{
					echo '<div class="col-xs-8 blackoutPictureLink typeBlock_300x335 front_card_double">';
					echo getCardSimpleDouble($post);
					echo '</div>';
				}
				
				$count++;
			}
			
			wp_reset_postdata();?>
			<div id="wr_btnMorePosts" class="col-xs-12 item-lastBtnBlk">
				<?php include 'php/btn_load_more_posts.php';?>
			</div>
	</div>
</div>

<?php get_footer(); ?>

