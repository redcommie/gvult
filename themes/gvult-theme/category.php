<?php 
	get_header();
	$catID = get_query_var('cat');
	$arrIDCat = $MAIN_CATEGORY;

// Возвращает название заголовка для страницы
	function ss_get_title_category( $catID )
	{
		if ( $catID ==  3 )
			return 'Каталог новых и лучших ресторанов в Киеве';
		else if ( $catID ==  5 )
			return 'Каталог новых и лучших шопингов в Киеве';
		else if ( $catID ==  6 )
			return 'Каталог новых и лучших баров и кафе в Киеве';
		else if ( $catID ==  7 )
			return 'Каталог новых и лучших продуктовых магазинов в Киеве';
		else if ( $catID ==  24 )
			return 'Каталог новых и лучших салонов красоты в Киеве';
		else if ( $catID ==  31 )
			return 'Каталог новых и лучших предложений для дома в Киеве';
		else if ( $catID ==  4 )
			return 'Каталог новых и лучших предложений для детей в Киеве';
		else if ( $catID ==  36 )
			return 'Каталог новых и лучших спорт площадок в Киеве';
		else if ( $catID ==  27 )
			return 'Каталог новых и лучших мест отдыха в Киеве';
		else
			return 'Такого не бывает';
	}

// Возвращает название заголока Топ 5
	function ss_get_top_title_category( $catID )
	{
		if ( $catID ==  3 )
			return 'Топ 5 ресторанов столицы';
		else if ( $catID ==  5 )
			return 'Топ 5 шопингов столицы';
		else if ( $catID ==  6 )
			return 'Топ 5 баров и кафе столицы';
		else if ( $catID ==  7 )
			return 'Топ 5 продуктовых магазинов столицы';
		else if ( $catID ==  24 )
			return 'Топ 5 салонов красоты столицы';
		else if ( $catID ==  31 )
			return 'Топ 5 предложений для дома столицы';
		else if ( $catID ==  4 )
			return 'Топ 5 предложений для детей столицы';
		else if ( $catID ==  36 )
			return 'Топ 5 спорт площадок столицы';
		else if ( $catID ==  27 )
			return 'Топ 5 мест отдыха столицы';
		else
			return 'Такого не бывает';
	}

// Возвращает название заголока типа "все рестораны города Киева"
	function ss_get_all_title_category( $catID )
	{
		if ( $catID ==  3 )
			return 'Все рестораны города Киева';
		else if ( $catID ==  5 )
			return 'Все шопинги города Киева';
		else if ( $catID ==  6 )
			return 'Все бары и кафе города Киева';
		else if ( $catID ==  7 )
			return 'Все продуктовые магазины города Киева';
		else if ( $catID ==  24 )
			return 'Все салоны красоты города Киева';
		else if ( $catID ==  31 )
			return 'Все предложения для дома города Киева';
		else if ( $catID ==  4 )
			return 'Все предложения для детей города Киева';
		else if ( $catID ==  36 )
			return 'Все спорт площадки города Киева';
		else if ( $catID ==  27 )
			return 'Все места отдыха города Киева';
		else
			return 'Такого не бывает';
	}

// Возвращает название заголока отзывов
	function ss_get_comment_title_category( $catID )
	{
		if ( $catID ==  3 )
			return 'Последние отзывы о ресторанах';
		else if ( $catID ==  5 )
			return 'Последние отзывы о шопингах';
		else if ( $catID ==  6 )
			return 'Последние отзывы о барах и кафе';
		else if ( $catID ==  7 )
			return 'Последние отзывы о продуктовых магазинах';
		else if ( $catID ==  24 )
			return 'Последние отзывы о салонах красоты';
		else if ( $catID ==  31 )
			return 'Последние отзывы для дома';
		else if ( $catID ==  4 )
			return 'Последние отзывы для детей';
		else if ( $catID ==  36 )
			return 'Последние отзывы о спорт площадках';
		else if ( $catID ==  27 )
			return 'Последние отзывы о местах отдыха';
		else
			return 'Такого не бывает';
	}

//  Ф-ция провеки наличия установленных дополнительных фильтров
	function check_add_filters()
	{
		if ( isset($_GET['district']) || isset($_GET['subway']) || isset($_GET['subcat']) || isset($_GET['openig']) || isset($_GET['address']) )
			return true;

		return false;
	}

	if ( isset($_GET['map']) )
		include 'php/pages/category-map.php';
	else
		include 'php/pages/category-list.php';
?>
<?php 
	get_footer(); 
?>