<?php 
	get_header();

	$links = getLinks();

	function get_count_find_posts()
	{
		$argsPlaces = array
		(	
			'post_type'				=> array('places'),
			's' 					=> $_GET['s']
		);

		$argsNews = array
		(	
			//'cat'				=> array('news', 'news-2'),
			's' 					=> $_GET['s'],
			'post_type'			=> 'post'
		);

		$argsReview = array
		(	
			'post_type'				=> array( 'review' ),
			's' 					=> $_GET['s']
		);

		// Формируем дополнительный запрос
			$addOpt = create_query_sort_filter($_GET);
			
			if ( !empty( $addOpt ) )
			{
				$argsPlaces = array_merge($argsPlaces, $addOpt);
				$argsNews = array_merge($argsNews, $addOpt);
				$argsReview = array_merge($argsReview, $addOpt);
			}
				

		$queryPlaces = new WP_Query($argsPlaces);
		$queryNews = new WP_Query($argsNews);
		$queryReviews = new WP_Query($argsReview);

		$result['places'] = $queryPlaces->found_posts;
		$result['news'] = $queryNews->found_posts;
		$result['reviews'] = $queryReviews->found_posts;
		$result['all'] = $queryPlaces->found_posts + $queryNews->found_posts + $queryReviews->found_posts;

		if ( $result['places'] != 0 )
			$result['postID'] = $queryPlaces->post->ID;
		else
			$result['postID'] = 0;

		return $result;
	}
?>

<div class="container-fluid">
	<div class="row result-row">
		<h1 class="addPlaceH1 col-sm-12">поиск по сайту:</h1>
		<div class=" col-sm-12">
			<form class="form-group search-form">
				<input type="text" name="fullsearch" id="search_string" class="search-input col-sm-9 col-xs-7" value="<?php echo $_GET['s'];?>" placeholder="Расширенный поиск по содержанию">
				<div class="class="col-sm-3 col-xs-5 margin_zero padding_zero">
					<a href="#" id="btn_search_page" class="btn-small-blue search-btn-blue <?php if (empty($_GET['s'])) echo 'noactive';?>">ИСКАТЬ</a>
				</div>
				<?php if(isset($_GET['add'])){
					?>
					<a id="btn_open_menu_advance_search" href="#" class="change-search-param change-search-param-hdn">Изменить параметры раширенного поиска</a>
				<?php }
				else{
					?>
					<a id="btn_open_menu_advance_search" href="#" class="change-search-param">Изменить параметры раширенного поиска</a><?php
				}?>
			</form>
			<?php if(isset($_GET['add'])){
			?><form><?php
			}
			else{
				?><form id="advance_param_search" class="advance_param_search"><?php
			}
			?>
			<!-- <form id="advance_param_search" class="advance_param_search"> -->
				<label>
				    <input class="checkbox" type="checkbox" name="checkbox-test">
				    <span class="checkbox-custom"></span>
				</label>
				<label class="exact-match-lbl">Точное совпадение всех слов</label>
				<div class="col-sm-12 col-xs-12 search-parametrs">
					<h2 class="addparam-head">Допонительные параметры:</h2>
					
						<div class="col-sm-2 col-xs-4 margin_zero padding_zero">
							<a href="" class="categoryFilterName sort_field <?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'date') ? 'active' : ''; ?>" data-sort="date">По дате</a>
						</div>
						<div class="col-sm-10 col-xs-8 margin_zero padding_zero">
							<a href="" class="categoryFilterName catFilterSorting sort_field <?php echo (isset($_GET['orderby']) && $_GET['orderby'] != 'date') ? 'active' : ''; ?>" data-sort="rating_total">По рейтингу</a>
						</div>
					
					<p class="search-par usial-bill">Средний чек:</p>
					<?php 
						$averageChecks = get_average_checks();
						for ( $i = 0; $i < count($averageChecks); $i++ )
						{
							echo '<p class="fullmapParametrs2 average_checks'
								.((isset($_GET['check']) && $_GET['check'] == $averageChecks[$i]['min']) ? ' active' : '')
								.'" data-min='.$averageChecks[$i]['min'].
								'>'
								.$averageChecks[$i]['name']
								.' ₴</p>';									
						}?>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<?php $categories = ss_get_parent_categories();
						if ( isset($_GET['cat']) )
						{
							$category = get_term($_GET['cat'], 'category');
							$catData = 'data-id="'.$_GET['cat'].'" value="'.$category->name.'"';
						}
						?>
					<label class="search-par">Основные категории</label>
					<input id="main_category" 
							type="text" 
							class="form-control  fullsearch-impform input_list" 
							placeholder="Выберите категорию" 
							<?php echo isset($catData) ? $catData : '';
							?>
							readonly>
					<span class="selectsrch-arrow-down"></span>
					<ul class="list addplace-selectlist filter_field">
						<?php
							echo '<li class="cancel_item_list" data-value="" data-id="">Отменить выбор</li>'; 
							for ( $i = 0; $i < count($categories);  $i++ )
							{ 
								echo '<li data-value="'.$categories[$i]->name.'" data-id="'.$categories[$i]->term_id.'">'.$categories[$i]->name.'</li>';
							}
						?>
					</ul>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<?php 
						if ( isset($_GET['subcat']) )
						{
							$category = get_term($_GET['subcat'], 'category');
							$subcatData = 'data-id="'.$_GET['subcat'].'" value="'.$category->name.'"';
						}
					?>
					<label class="search-par">Подкатегории</label>
					<span class="selectsrch-arrow-down"></span>
					<input id="sub_category" 
						type="text" 
						class="form-control fullsearch-impform input_list"
						placeholder="Выберите категорию" 
						<?php echo isset($subcatData) ? $subcatData : '';?>
						readonly>
				</div>
				<div class="col-sm-4 search-empty-block search-forms-blocks">
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Район города</label>
					<span class="selectsrch-arrow-down"></span>
					<input id="input_district" 
						class="form-control fullsearch-impform input_list" 
						type="text" 
						placeholder="Выберите район"
						value="<?php echo isset($_GET['district']) ? $_GET['district'] : '';?>"
						data-value="<?php echo isset($_GET['district']) ? $_GET['district'] : '';?>"
						readonly>
					<ul class="list filter_field">
						<?php 
							$districts = get_districts_name();
							echo '<li class="cancel_item_list" data-value="" data-id="">Отменить выбор</li>';
							for ( $i = 0; $i < count($districts);  $i++ )
							{ 
								echo '<li data-value="'.$districts[$i].'">'.$districts[$i].'</li>';
							}
						?>
					</ul>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="addNewPlaceFont">Ближайшая станция метро</label>
				    <input id="subway" 
				    	class="form-control fullsearch-impform input_list" 
				    	type="text" 
				    	placeholder="Выберите станцию"
				    	value="<?php echo isset($_GET['subway']) ? $_GET['subway'] : '';?>"
						data-value="<?php echo isset($_GET['subway']) ? $_GET['subway'] : '';?>"
				    	readonly>
				    <span class="selectsrch-arrow-down"></span>
					<ul class="list filter_field">
						<?php 
							$subways = get_subway_names();
							echo '<li class="cancel_item_list" data-value="" data-id="">Отменить выбор</li>';
							for ( $i = 0; $i < count($subways);  $i++ )
							{ 
								echo '<li data-value="'.$subways[$i].'">'.$subways[$i].'</li>';
							}
						?>
					</ul>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Укажите улицу</label>
					<input id="input_address" 
						type="text" 
						name="searchstreet" 
						class="form-control border_radius_zero search-forms"
						value="<?php echo isset($_GET['address']) ? $_GET['address'] : '';?>"
						data-value="<?php echo isset($_GET['address']) ? $_GET['address'] : '';?>"
						>
				</div>
				<div class="col-sm-12">
					<div>
						<label>
						<input id="checkbox_place_open" 
							class="checkbox" 
							type="checkbox" 
							name="checkbox-test" 
							data-type="openig" 
							data-checked="<?php echo isset($_GET['openig']) ? $_GET['openig'] : '';?>"
							<?php echo ((isset($_GET['openig']) && $_GET['openig'] == 'true') ? 'checked' : '');?>>
					    <span class="checkbox-custom"></span>
					    <span class="fullsch-ckb-sp1">Заведение открыто</span>
						</label>
					</div>
					<div>
						<label>
							    <input class="checkbox1" type="checkbox" name="checkbox-test">
							    <span class="checkbox-custom1"></span>
							    <span class="fullsch-ckb-sp2">Наличие 3D-тура</span>
						</label>
					</div>
				</div>
				<div class="col-sm-12 fullsrchbtn-block">
					<a id="btn_additional_search" href="#" class="btn-small-blue fullsearch-btn">НАЧАТЬ ПОИСК</a> 
					<a href="#" class="btn-standart-grey fullsearch-btn-grey">СБРОСИТЬ</a>
				</div>
			</form>
		</div>
		<div class="col-sm-12 rst-param-block <?php if(isset($_GET['add'])){?> rst-param-hidden<?php } ?>">
			<div class="search-param-blockss">
				<div class="col-sm-2 col-xs-3 margin_zero padding_zero">
					<a href="<?php echo isset($_GET['orderby']) && $_GET['orderby'] == 'date' ? $links['without_date'] : $links['date'];?>" class="categoryFilterName categoryFilterNameoriesryFilterNameSorting <?php echo isset($_GET['orderby']) && $_GET['orderby'] == 'date' ? 'active' : '';?>">По дате</a>
				</div>
				<div class="col-sm-2 col-xs-4 margin_zero padding_zero">
					<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? $links['without_rating'] : $links['rating'];?>" class="categoryFilterName catFilterSorting <?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? 'active' : '';?>">По рейтингу</a>
				</div>
				<div class="col-sm-8 col-xs-5 margin_zero padding_zero">
					<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? $links['without_check'] : $links['check'];?>" class="categoryFilterName catFilterSorting <?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? 'active' : '';?>">По среднему чеку</a>
				</div>
			</div>
			<?php $countPosts = get_count_find_posts();?>
			<div class="results-number-block">
				<a href=<?php echo $links['type']['all'];?> 
					class="results-par <?php echo ((!isset($_GET['type']) || $_GET['type'] == 'all')  ? 'active' : '')?>" 
					title="Все результаты">ВСЕ РЕЗУЛЬТАТЫ <span class="results-number"><?php echo $countPosts['all'];?></span></a>
				<a href="<?php echo $links['type']['places'];?>" 
					class="results-par <?php echo ((isset($_GET['type']) && $_GET['type'] == 'places')  ? 'active' : '')?>" 
					title="Заведения">ЗАВЕДЕНИЯ <span class="results-number"><?php echo $countPosts['places'];?></span></a>
				<a href="<?php echo $links['type']['post'];?>" 
					class="results-par <?php echo ((isset($_GET['type']) && $_GET['type'] == 'post')  ? 'active' : '')?>" 
					title="Новости">НОВОСТИ <span class="results-number"><?php echo $countPosts['news'];?></span></a>
				<a href="<?php echo $links['type']['review'];?>" 
					class="results-par <?php echo ((isset($_GET['type']) && $_GET['type'] == 'review')  ? 'active' : '')?>" 
					title="Статьи">СТАТЬИ <span class="results-number"><?php echo $countPosts['reviews'];?></span></a>
			</div>
		</div>
	</div>
	<?php if(isset($_GET['add'])){
		?><div class="resultblock-hidden"><?php
		}
		else{
			?><div class="resultblock-visible"><?php
		}?>
		<div class="row result-row results-map-block">
		<?php
			if ( $countPosts['postID'] != 0 )
			{
				echo '<a href="#" class="col-sm-4 col-xs-12 first-result-card-phone post" data-id="'.$countPosts['postID'].'">';
				echo getFirstSearchCard($countPosts['postID']);
				echo '</a>';	
			}?>
			<div class="col-sm-8 col-xs-12 result-rest-map padding_zero">
				<div id="map" class="search-map"></div>
				
			</div>
		</div>	
		<div class="row result-row result-cards-block-phone">
			<?php
			$temp = $wp_query;
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$wp_query = null;
			if(isMobile()){
				$posts_per_page = 4;
			}
			else{
				$posts_per_page = $paged == 1 ? 10 : 9;
			}
			$post_type = (isset( $_GET['type'] ) && $_GET['type'] != 'all')
						? array($_GET['type'])
						: array('places', 'post', 'review');

			$args = array
				(
					'posts_per_page'	=> $posts_per_page,
					'post_type'			=> $post_type,
					's' 				=> $_GET['s'],
					'paged'             => $paged						
				);

			// Формируем дополнительный запрос
				$addOpt = create_query_sort_filter($_GET);
				
				if ( !empty( $addOpt ) )
					$args = array_merge($args, $addOpt);

			$wp_query = new WP_Query($args);


			while ( $wp_query ->have_posts() )
			{
				$wp_query ->the_post();
					
				if( $post->ID == $countPosts['postID'] ) continue;

				echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335 post" data-id="'.$post->ID.'">';
				echo getCardSimpleSingle($post, false, 'resultCard');
				echo '</div>';
			}


			echo '<div class="col-sm-12 col-xs-12">
				<div class="fullmap-pagination">';
			the_posts_pagination( 
				array(
				'mid_size' => 2,
				'show_all'     => false,
				'prev_next'    => true,
				'prev_text'    => '',
				'next_text'    => ''
				) 
			);
			echo '</div>
				</div>';
			$wp_query= $temp;
			wp_reset_postdata();?>
		</div>
	</div>
	<?php if(isset($_GET['add'])){
	?><div class="fullsearch-cardblock">
	<?php }
		else{
		?><div class="fullsearch-cardblock-hdn"><?php
	}?>
		<div class="col-sm-12 bigHeader">
			<p class="ntfnd-big-parag title_most_popular">самое</p>
		</div>

		<?php
		$args = array
		(
			'posts_per_page'	=> 3,
			'offset'			=> 20,
			'post_type'			=> array('post', 'places')
		);

		$query = new WP_Query($args);
		$count = 0;
		
		while ( $query->have_posts() )
		{
			$query->the_post();
			
			if ( $count != 3 )
			{
				echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335">';
				echo getCardSimpleSingle($post);
				echo '</div>';
			}
			
			$count++;
		}
		
		wp_reset_postdata();?>
	</div>
</div>

<?php 
	get_footer();
	
?>