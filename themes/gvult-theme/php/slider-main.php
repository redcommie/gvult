
<div id="myCarousel" class="carousel slide index-car main_slider" data-ride="carousel">

	  <ol class="carousel-indicators">
	    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	    <li data-target="#myCarousel" data-slide-to="1"></li>
	    <li data-target="#myCarousel" data-slide-to="2"></li>
	    <li data-target="#myCarousel" data-slide-to="3"></li>
	    <li data-target="#myCarousel" data-slide-to="4"></li>
	    <li data-target="#myCarousel" data-slide-to="5"></li>
	  </ol>
					
<div class="carousel-inner" role="listbox">
<?php
	$countPost = 6;
	$categoryID = get_query_var('cat');
	if(is_front_page()){
		$args = array(
			'posts_per_page'	=> $countPost,
			'post_type'			=> array('post', 'review', 'places') 
		);
	}
	else if (is_category(array('city-guide', 'news-2', 'culture', 'fashion_beauty', 'gastro_life')))
	{
		$args = array(
			'posts_per_page'	=> $countPost,
			'post_type'			=> array('review', 'post', 'places'),
			//'cat'				=> $categoryID
		);
	}
	else
	{
		$args = array(
			'posts_per_page'	=> $countPost,
			'post_type'			=> array('review', 'post', 'places'),
		);	
	}

	
	


		$gvPost = new WP_Query($args);

		if ( $gvPost->have_posts() ) {

			for($i = 0; $i < $countPost; $i++){
					$gvPost->the_post();					
						?>
							<div class="item <?php if (!$i) echo 'active';?> ind-slide1-item">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"  class="ind-slide-img">
							    <div class="carousel-caption indcar-capt1">
									<a href="<?php echo get_the_permalink() ?>"><span><?php the_title(); ?></span></a>
								</div>
						   </div>   
						 <?php
			}

		}

		wp_reset_postdata();
?>
</div>


	  <a class="left carousel-control main-slider-controlers" href="#myCarousel" role="button" data-slide="prev" style="background-image: url(<?php echo get_the_post_thumbnail_url($gvPost->posts[$countPost-1]->ID)?>)">
	    <span class="glyphicon glyphicon-chevron-left slider-arrow-left" aria-hidden="true"></span>
	    <span class="arrow_shadow"></span>
	  </a>
	  <a class="right carousel-control main-slider-controlers" href="#myCarousel" role="button" data-slide="next" style="background-image: url(<?php echo get_the_post_thumbnail_url($gvPost->posts[1]->ID)?>)">
	    <span class="glyphicon glyphicon-chevron-right slider-arrow-right" aria-hidden="true"></span>
	    <span class="arrow_shadow"></span>
	    </a>
</div>