<div class="container-fluid">
    <div class="col-xs-12 cityGuideHeader-block">
		<div class="col-xs-12">
			<h1 class="cityGuideHeader">каталог лучших заведений киева</h1>
		</div>
	</div>
	<div class="row city-guide-slider">
		<?php include ROOT_PATH.'/php/slider-main.php';?>
	</div>
	<hr>
	<div class="row wr_switcher">
		<div class="col-sm-offset-9 col-sm-3 catFiltMapSwitcherBlock">
			<p class="catFiltMapSwitcherPar">Списком</p>
			<div id="switcher_list_map" class="catFiltMapSwitcherImg" data-switch="list"><span></span></div>
			<p class="catFiltMapSwitcherPar">На карте</p>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 padding_zero">
		<?php include ROOT_PATH.'/php/card-places.php';?>
	</div>
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="restParagrafHead col-sm-12 col-xs-12">
				<p class="paragraf">Последние новости</p>
			</div>
			<?php
				$args = array
					(
						'posts_per_page'		=> 3,
						'post_type'				=> array('post'),
						'cat'					=> array(613, 39)
					);

				$query = new WP_Query($args);
				
				$count = 0;
				while ( $query->have_posts() )
				{
					$query->the_post();
											
					if ( $count != 3 )
					{
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335">';
						echo getCardSimpleSingle($post, false);
						echo '</div>';
					}
					
					$count++;
				}
					
				wp_reset_postdata();
			?>
			<div class="row">
				<div class="col-sm-12 col-xs-12 top5place-container">
					<div class="col-sm-12 col-xs-12 top5place-container-two">
						<div class="col-sm-4 col-xs-12 cityGuideTop5Block margin_zero padding_zero">
							<p class="top5TableHead">Топ 5 заведений</p>
							<div id="container_name_category" class="col-sm-7 col-xs-7">
								<?php echo get_list_main_category('cityGuideTableName', 'guide');?>
							</div>
							<div class="col-sm-5 col-xs-5">
								<?php echo get_list_count_post_main_category( 'cityGuideTableValue' );?>
							</div>	
						</div>
						<div id="container_top_cards" class="col-sm-8 col-xs-12 margin_zero padding_zero">
							<?php echo get_top_five_cards_places( 3 );?>
							<div class="col-sm-4 col-xs-6 city-guide-addplaceblock1">
								<div class="similarPlaceCard city-guide-addplaceblock">
									<p class="simPlaceParagraf simPlaceParagraf1">Нет вашего заведения?</p>
									<img src="<?php echo get_template_directory_uri().'/img/add_new_one_icon.png'?>" class="simPlaceAddImage" alt="Добавить свое заведение">
									<a href="<?php echo home_url().'/dobavit-zavedenie'?>" class="btn-small-blue simPlaceAddBtnBlue">ДОБАВИТЬ</a>
								</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 cityGuideLastNews">
			<div class="col-sm-8">
				<div class="restParagrafHead cityGuideRewiewHead">
					<p class="paragraf">Последние отзывы</p>
					<a href="#" class="btn-standart-border cityGuideRewiewBtn1">НАПИСАТЬ ОТЗЫВ</a>

				</div>
				<div>
					<?php
						$revCount;
						if(isMobile()){
							$revCount = 4;
						}
						else{
							$revCount = 8;
						}
						echo ss_getComments($revCount, 0, 3, 'col-sm-6 ctg-RewiewBlocks');
					?>
					<div id="wr_btnMoreComments" class="cityGuideMoreViewBtnBlock col-sm-12">
					    <a id="btnMoreComments" href="#" 
					    	class="cityGuideMoreViewBtn"
					    	data-maxcountcomments="500"
							data-offset="8"
							data-countcomments="8"
							data-catid=""
							data-classes="col-sm-6 category-commentBlocks">
					      <span class="MoreViewBtnPar">ПОКАЗАТЬ ЕЩЕ</span>
					      <span  class="MoreViewBtPoint"></span>
					      <span class="MoreViewBtPoint"></span>
					      <span class="MoreViewBtPoint"></span>
					    </a>
	  				</div>
				</div>
			</div>
			<div class="col-sm-4 cityGuideRewiewBanner">
				<a href="#">
					<img src="http://lorempixel.com/300/600"  class="img-responsive" alt="<?php the_title(); ?>">
					
				</a>
			</div>
		</div>
	</div>
	<div class="row city-guide-addbanner">
		<div class="restAddPlaceBanner col-sm-12">
				<p class="restAddPlaceBannerHead">Добавьте своё заведение</p>
				<p class="restAddPlaceBannerPar">Если вы владелец магазина, ресторана или коворкинг площадки, добавьте свое заведение на «Гвалт».</p>
				<a href="<?php echo home_url().'/dobavit-zavedenie'?>" class="btn-standart-border restAddPlaceBannerBtn">ДОБАВИТЬ</a>
		</div>
	</div>
	<div class="row cityGuideNewPlaceBlock">
		<div class="col-sm-12">
			<div class="col-sm-8 margin_zero padding_zero">
					<div class="restParagrafHead cityGuideNewPlacesHead col-sm-12">
						<p class="paragraf">Новые заведения</p>
					</div>
					<?php
						$args = array
							(
								'posts_per_page'	=> 6,
								'post_type'			=> 'places'
							);

							$query = new WP_Query($args);
							$count = 0;
							
							while ( $query->have_posts() )
							{
								$query->the_post();
								
								if ( $count != 6 )
								{
									?><div class="col-sm-4 col-xs-6"><?php 
									echo getCardPlace($post);
									?></div><?php
								}


								$count++;
							}
							
		
							wp_reset_postdata();?>
					<div id="wr_btnMorePlaces" class="col-sm-12 col-xs-12 margin_zero padding_zero categoryFilterViewMoreBtn <?php echo $query->found_posts <= 15 ? 'hide' : ''?>">
						<a id="btnMorePlaces" 
							href="#" 
							class="cityGuideMoreViewBtn" 
							data-maxcountposts="0" 
							data-offset="14" 
							data-posttype="<?php echo 'places';?>"
							data-countposts="9"
							data-type='place'>
							<span class="MoreViewBtnPar">ПОКАЗАТЬ ЕЩЕ</span>
							<span class="MoreViewBtPoint"></span>
							<span class="MoreViewBtPoint"></span>
							<span class="MoreViewBtPoint"></span>
						</a>
					</div>
				</div>
			<div class="col-sm-4 phone-ct-guide-banner">
					<div class="ct-guide-banner">
						<a href="#">
						 	<img src="http://placehold.it/300x600" class="img-responsive" alt="<?php the_title(); ?>">
						</a>
					</div>	
				</div>
		</div>
	</div>		
</div>