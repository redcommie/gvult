<div class="container-fluid">
	<div class="col-sm-12">
		<div class="col-sm-12">
			<h1 class="cityGuideHeader"><?php echo ss_get_title_category( $catID );?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 cat-places-paramer1">
			<div class="col-sm-offset-9 col-sm-3 catFiltMapSwitcherBlock">
				<p class="catFiltMapSwitcherPar">Списком</p>
				<div id="switcher_list_map" class="catFiltMapSwitcherImg turn_map" data-switch="map"><span></span></div>
				<p class="catFiltMapSwitcherPar">На карте</p>
			</div>
		</div>
	</div>
	<div class="row container_map">
		<div id="map" class="category_map" data-catid="<?php echo $catID?>"></div>
		<div id="filter_block" class="col-sm-4 categoryFilterBlock map_filter_block">
			<div class="col-sm-12">
				<p class="categoryFilterBlockHead">Категории заведений</p>
			</div>
			<div class="col-sm-7">
				<?php echo get_list_main_category('categoryFilterName', '', true);?>
			</div>
			<div class="col-sm-5 categoryFilterValueBlock">
				<?php echo get_list_count_post_main_category( 'categoryFilterValue' );?>
			</div>
			<p id="btn_additional_sort" class="col-sm-12 btn_additional_sort" <?php if (check_add_filters()) echo 'style="display: none;"'; ?>>Дополтинельно</p>
			<form id="form_additional_sort" class="col-sm-12 form_additional_sort" <?php if (!check_add_filters()) echo 'style="display: none;"'; ?>>
				<div>
					<label>
					    <input id="checkbox_place_open" class="checkbox" type="checkbox" name="checkbox-test" data-type="openig" <?php echo ((isset($_GET['openig']) && $_GET['openig'] == 'true') ? 'checked' : '');?>>
					    <span class="checkbox-custom"></span>
					    <span class="fullsch-ckb-sp1">Заведение открыто</span>
					</label>
				</div>
				<div>
					<label>
					    <input class="checkbox1" type="checkbox" name="checkbox-test">
					    <span class="checkbox-custom1"></span>
					    <span class="fullsch-ckb-sp2">Наличие 3D-тура</span>
					</label>
				</div>
				<div class="wr_sub_category">
					<span></span>
					<input id="sub_category" type="text" class="form-control categoryFilterForms  input_list" 
						placeholder="<?php echo (isset($_GET['subcat']) ? get_cat_name($_GET['subcat']) : 'Подкатегории');?>" readonly>
					<ul class="list">
						<?php 
							$categories = ss_get_sub_categories( $catID );

							for ( $i = 0; $i < count($categories);  $i++ )
							{ 
								echo '<li data-value="'.$categories[$i]['name'].'" data-id="'.$categories[$i]['id'].'" data-type="subcat">'.$categories[$i]['name'].'</li>';
							}
						?>
					</ul>
				</div>
				<div class="wr_sub_category">
					<input id="subway" class="form-control categoryFilterForms input_list" type="text" readonly placeholder="<?php echo (isset($_GET['subway']) ? $_GET['subway'] : 'Станция метро');?>">
				    <span></span>
					<ul class="list">
						<?php 
							$subways = get_subway_names();
							for ( $i = 0; $i < count($subways);  $i++ )
							{ 
								echo '<li data-value="'.$subways[$i].'" data-type="subway">'.$subways[$i].'</li>';
							}
						?>
					</ul>
				</div>
				<div class="wr_sub_category">
					<input id="input_district" class="form-control categoryFilterForms input_list" type="text" readonly placeholder="<?php echo (isset($_GET['district']) ? $_GET['district'] : 'Район города');?>">
				    <span></span>
					<ul class="list">
						<?php 
							$districts = get_districts_name();
							for ( $i = 0; $i < count($districts);  $i++ )
							{ 
								echo '<li data-value="'.$districts[$i].'" data-type="district">'.$districts[$i].'</li>';
							}
						?>
					</ul>
				</div>
				<div class="fillYourStreetInput categoryFilterForms">
					<p>Укажите улицу</p>
					<span class="wall_hide"></span>
					<select id="select-address" class="form-control categoryFilterForms field_street"></select>
					<!-- <input type="text" class="form-control categoryFilterForms field_street"> -->
				</div>
			</form>
		</div>
	</div>
	<div class="row contentDiv7">
		<h3>Новости</h3>
		<?php
		$args = array
			(
				'posts_per_page'	=> 3,
				'offset'			=> 0,
				'cat'				=> '39,612'
			);

		$query = new WP_Query($args);
		while ( $query->have_posts() )
		{
			$query->the_post();
								
			echo '<div class="col-xs-4 blackoutPictureLink typeBlock_300x335">';
			echo getCardSimpleSingle($post);
			echo '</div>';
		}
		
		wp_reset_postdata();?>
	</div>
</div>
