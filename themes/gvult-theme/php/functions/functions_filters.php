<?php
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
  /*
  Вид базового шаблона:
  <nav class="navigation %1$s" role="navigation">
    <h2 class="screen-reader-text">%2$s</h2>
    <div class="nav-links">%3$s</div>
  </nav>
  */

  return '
    <nav class="navigation search-res-pag %1$s" role="navigation">
      <div class="nav-links">%3$s</div>
    </nav>';
}

// Фильтры перезаписи шаблонного письма
  add_filter('password_change_email', 'send_recovery_pass', 10, 3);
  function send_recovery_pass( $pass_change_email, $user, $userdata )
  {
    $userMeta = get_metadata('user', $user['ID']);
    $userPass = $userMeta['password'][0];

    delete_metadata('user', $user['ID'], 'password');

    $str = explode('.', $pass_change_email['message']);
    echo json_encode( $pass_change_email );

    $pass_change_email['message'] = $str[0].".\n\nВаш новый пароль:\n".$userPass.$str[1];

    return $pass_change_email;
  }

  add_filter( 'wp_mail_from', 'change_email_adress' );
  function change_email_adress( $email_address )
  {
    return 'navshop@ukr.net';
  }

  add_filter( 'wp_mail_from_name', 'change_email_from' );
  function change_email_from( $email_from )
  {
    return 'Портал Gvult';
  }

// Действие перезаписывает тип поста основного запроса на стр. city-guide map  
add_action('pre_get_posts', 'update_main_query', 10, 1);
function update_main_query( $obj )
{
 if ( $obj->get('category_name') == 'city-guide' && 
      $obj->is_main_query() && 
      strpos($_SERVER['QUERY_STRING'], 'maps') !== false
    )
 {
    $obj->set('post_type', 'places');
 }
}
?>