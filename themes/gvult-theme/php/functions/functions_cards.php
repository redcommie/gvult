<?php
//Карточки 
  function getCardSimpleSingle($post, $boolCategoryName = true, $resultCard = 'pageCard')
  { 

    $category = get_the_category($post->ID);

    $date = get_the_date('d F Y H:i', $post);
    if ( !empty($category) )
        $categoryName = $category[0]->category_parent
          ? get_cat_name($category[0]->category_parent) 
          : $category[0]->name;
    
    $content = '<a href="'.get_permalink($post->ID).'" class="">
                    <figure class="card_simple_figure">
                        <div class="blockPictureDate wr_img_card_simple_single" ';
    $content .= $boolCategoryName ? '' : 'style="margin-bottom: 20px;"';
    $content .= '><img src="'.get_the_post_thumbnail_url($post->ID).'" class="img-responsive"  alt="'.get_the_title($post->ID).'">';
    if($resultCard === 'pageCard'){
        $content .= '<div class="shadow_card"><p class="blockHiddenDate">'. $date .'</p></div>
                        </div>
                      <figcaption>';
        $content .= $boolCategoryName ? '<h5 class="cardHearedH5">'.(!empty($categoryName) ? $categoryName : '').'</h5>'
                      : '<h5 class="non_seen_head" >'.$categoryName.'</h5>';
        $content .= '<p class="paragraf">'.get_the_title($post->ID).'</p>
                          </figcaption>
                        </figure>
                      </a>';
    }
    elseif($resultCard ==='resultCard'){

        $postTime = get_the_date('j F Y H:i');
        $content .= '</div>
                      <figcaption>';
        $content .= '<h5 class="result-card-time">'.$postTime.'</h5>';
        $content .= '<p class="paragraf">'.get_the_title($post->ID).'</p>
                          </figcaption>
                        </figure>
                      </a>';
    }                 
    return $content;
  }

  function getCardSimpleDouble($post)
  {
    
    $date = get_the_date('d F Y H:i', $post);
    $category = get_the_category($post->ID);
    $categoryName = $category[0]->category_parent
      ? get_cat_name($category[0]->category_parent) 
      : $category[0]->name;

    $content = '<a href="'.get_permalink($post->ID).'"class="double_card_post">
                  <figure>
                    <div class="blockPictureDate wr_img_card_simple_double">
                      <img src="'.get_the_post_thumbnail_url($post->ID).'" class="img-responsive" alt="'.get_the_title($post->ID).'">
                     <div class="shadow_card"> <p class="blockHiddenDate">'. $date .'</p></div>
                    </div>
                    <figcaption>
                      <h5 class="cardHearedH5">'.$categoryName.'</h5>
                      <p class="paragraf1">'.get_the_title($post->ID).'</p>
                    </figcaption>
                  </figure>
                </a>';

     return $content;
  }

  function getCardSimpleDoubleWithBorder($post)
  { 
     $date = get_the_date('d F Y H:i', $post);
    $category = get_the_category($post->ID);
    $categoryName = $category[0]->category_parent
      ? get_cat_name($category[0]->category_parent) 
      : $category[0]->name;

    $content = '<a href="'.get_permalink($post->ID).'" class="">
                  <figure class="divWithBlueBorder">
                      <div class="blockPictureDate wr_img_card_simple_double_border">
                        <img src="'.get_the_post_thumbnail_url($post->ID).'" class="img-responsive imgBorder" alt="'.get_the_title($post->ID).'">
                        <div class="shadow_card"><p class="blockHiddenDate">'.$date.'</p></div>
                      </div>
                    <figcaption class="borderDescription">
                      <h5 class="cardHearedH5">'.$categoryName.'</h5>
                      <p class="paragraf2">'.get_the_title($post->ID).'</p>
                    </figcaption>
                  </figure>
                </a>';

     return $content;
  }

  function getCardSimpleTriple($post)
  {
    $date = get_the_date('d F Y H:i', $post);
    $category = get_the_category($post->ID);
    $categoryName = $category[0]->category_parent
      ? get_cat_name($category[0]->category_parent) 
      : $category[0]->name;

    $content = '<a href="'.get_permalink($post->ID).'" class="triple_card_category">
                  <figure>
                    <div class="blockPictureDate wr_img_card_simple_double">
                      <img src="'.get_the_post_thumbnail_url($post->ID).'" class="img-responsive" alt="'.get_the_title($post->ID).'">
                      <div class="shadow_card"><p class="blockHiddenDate">'.$date.'</p></div>
                    </div>
                    <figcaption>
                      <h5 class="cardHearedH5">'.$categoryName.'</h5>
                      <p class="paragraf1">'.get_the_title($post->ID).'</p>
                    </figcaption>
                  </figure>
                </a>';

     return $content;
  }
  
  function getCardPlace($post)
  {
    // $category = get_the_category($post->ID);
    $placeRating = get_post_meta($post->ID, 'rating_total', true);

    // if ( empty($category) )
    //   $categoryName      

    // $categoryName = $category[0]->category_parent
    //     ? get_cat_name($category[0]->category_parent) 
    //     : $category[0]->name;

    $content = '<a href="'.get_permalink($post->ID).'">
                    <figure class="similarPlaceCard">
                    <div class="similarPlaceImgBlock">
                      <img src="'.get_the_post_thumbnail_url($post->ID).'" class="simPleceMainImg img-responsive" alt="' .get_the_title($post->ID). '">';
                        
    if ( check_opening_place($post->ID) )
      $content = $content .'<span class="simPlaceLockIcon-open"></span>';
    else
      $content = $content . '<span class="simPlaceLockIcon"></span>';
                            
              
                        
    $content = $content .'<div class="simPlaceStarsBlock">';

                      for($i=0; $i<5; $i++){
                        if($placeRating-1 < $i){
                          $content = $content . '<span class="simPlaceStarIcon"></span>';
                        }
                        else{
                          $content = $content . '<span class="simPlaceStarIcon simPlaceeBlueStarIcon"></span>';
                        }
                      }

    $content = $content .'</div>
                    </div>
                    <figcaption class="simPlaceParagraf">'.get_the_title($post->ID).'</figcaption>
                  </figure>
                 </a>';

    return $content;
  }

//Функция вывода первой карточки результата поиска
function getFirstSearchCard($postID)
{
  $imgSourse = get_the_post_thumbnail_url($postID);
  $postTitle = get_the_title($postID);
  
  $content = '<div class="result-rest-card">
                      <div class="result-card-blimg">';
      
  if ( check_opening_place($postID) )
    $content = $content . '<span class="rescard-key"><span class="placecard-openIcon"></span></span>';
  else
    $content = $content .'<span class="rescard-key"><span class="placecard-lockIcon"></span></span>';

  $content .= '<img src="'.$imgSourse.'" class="img-responsive ">
    </div>
      <p class="result-rest-name">'.$postTitle.'</p>
  </div>';
                  
  return $content;

}

?>