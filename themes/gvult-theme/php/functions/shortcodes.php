<?php
//shortcode
//[populartheme]
function add_populartheme(  ){
  global $wp_query;
    $content = '<div class="artcl-quote3">
          <span class="quote3-main">Популярное по теме:</span>';

    if(get_the_category( $wp_query->post->ID )[0]->parent != 0){
      $postParent = get_the_category( $wp_query->post->ID )[0]->parent;
    }
    else{
      $postParent = get_the_category( $wp_query->post->ID )[0]->cat_ID;
    }

      $args = array
      (
        'posts_per_page'  => 1,
        'post_type'     => 'post',
        'orderby'       => 'meta_value_num',
        'meta_key'      => 'rating_total',
        'cat'       => $postParent
      );

      $query = new WP_Query($args);
      $count = 0;
      while ( $query->have_posts()){
        $query->the_post();

        if ( $count != 5 ){
          
          $link = get_the_permalink();
          $title = get_the_title();
          $content .= '<a href="' . $link .'"><span class="quote3-text">'
           . ' ' . $title . '</span></a>';
        }
      $count++;
      }
      wp_reset_postdata();
      $content = $content .  '</div>';

return $content;
}

add_shortcode('populartheme', 'add_popularTheme');



//shortcode
//[quote]
function add_blockquote($attr = array('author' => ''),$quote ){   
    $author = $attr['author'];
    $content = '<div class="article-quote1">
                    <p class="quote-text">' . $quote . '</p> <p class="quote-author">' . $author . '</p>
                </div>';
    return $content;
}

add_shortcode('quote', 'add_blockquote');


//shortcode
//[gallery]
add_filter('post_gallery', 'my_gallery_output', 10, 2);
function my_gallery_output( $output, $attr ){
  $ids_arr = explode(',', $attr['ids']);
  $ids_arr = array_map('trim', $ids_arr );

  $pictures = get_posts( array(
    'posts_per_page' => -1,
    'post__in'       => $ids_arr,
    'post_type'      => 'attachment',
    'orderby'        => 'post__in',
  ) );

  if( ! $pictures ) return 'Запрос вернул пустой результат.';

  $out = '<div id="myCarousel-item" class="carousel slide item-slide-gallery" data-ride="carousel gallery_photos">
   <div class="carousel-inner">';

   for($i=0; $i<count($pictures);$i++){
    $src = $pictures[$i]->guid;
    $out .=  '<div class="item';
    $out .= $i ? '">' : ' active">';
    $out .= '<div class="slider-background" style="background-image:url(' .$src. ');"></div></div>';
    
    }
 $out .= '</div>
  <a class="left carousel-control artsld-arrows" href="#myCarousel-item" data-slide="prev">
    
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    
  </a>
  <a class="right carousel-control artsld-arrows" href="#myCarousel-item" data-slide="next">
   
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
   
  </a>
</div>';
  return $out;
}

// shortcode
// [gallery2]
function my_gallery_two_output( $attr ){
  $ids_arr = explode(',', $attr['ids']);
  $ids_arr = array_map('trim', $ids_arr );

  $pictures = get_posts( array(
    'posts_per_page' => -1,
    'post__in'       => $ids_arr,
    'post_type'      => 'attachment',
    'orderby'        => 'post__in',
  ) );

  if( ! $pictures ) return 'Запрос вернул пустой результат.';

   $out = '<div class="carousel-wrap-btn">
            <div class="owl-carousel owl-theme">';

   for($i=0; $i<count($pictures);$i++){
    $src = $pictures[$i]->guid;
    $out.='<div class="gallery2-blocks" style="background-image: url('.$src.');"></div>';
  }
 $out.= '</div>';
    
      $numPic = count($pictures);        
      $out .=  '<a id="btnShowModal" title="Show modal window" href="#" class="slider2-btn">
                        <span>Все</span>
                        <span>' .$numPic. '</span>
                 </a>
              </div>';
  return $out;
}
add_shortcode('gallery2', 'my_gallery_two_output');
?>