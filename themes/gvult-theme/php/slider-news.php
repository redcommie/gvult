
<div id="myCarousel1" class="carousel slide newsFirstDiv" data-ride="carousel">
	<h4>Последние новости</h4>
					
	<div class="carousel-inner " role="listbox">

						 
		

<?php
	$args = array(
			'posts_per_page'	=> 9,
			'post_type'			=> array('post', 'review', 'places'),
			'cat'				=> array(613, 39),
			'offset'			=> 50			 
		);
	


		$gvPost = new WP_Query($args);


		if ( $gvPost->have_posts() ) {

			for($i = 0; $i < 3; $i++){

				?> <div class="item <?php if (!$i) echo 'active';?>">
				<?php
				for($j = 0; $j < 3; $j++){
						$gvPost->the_post();
						?>
							
							<a href="<?php echo get_post_permalink($post);?>">
								<figure class="moreNewsblocks">
									<div class="slider_news_img_block">
										<div></div>
										<img src="<?php echo get_the_post_thumbnail_url() ; ?>" class="img-responsive categoryImagesNews slider2-img">
									</div>
									<figcaption><?php the_title(); ?></figcaption>
								</figure>
							</a>
								
						 <?php
				}
				?>
				</div>

				<?php
			}
		}

?>

	</div>
		<div class=" news-block-buttons">
			<a href="<?php echo get_category_link(613); ?>" class="btn-standart-border catfilter-more-news-btn">БОЛЬШЕ НОВОСТЕЙ</a>
			<div>	
				<a class="more-news-switchrs carousel-control left" href="#myCarousel1" role="button" data-slide="prev">
					<img src="<?php bloginfo('template_url'); ?>/img/right-arrow.png" alt="switch">
				</a>
				<a class=" more-news-switchrs carousel-control right" href="#myCarousel1" role="button" data-slide="next">
					<img src="<?php bloginfo('template_url'); ?>/img/left-arrow.png" alt="switch">
				</a>
			</div>
		</div>	
</div>