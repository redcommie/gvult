<?php
	get_header();
?>
<div class="container-fluid">
	
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-12">
				<h1 class="addPlaceH1 col-sm-12">поиск по сайту:</h1>
				
				<div class=" col-sm-12">
					<form class="form-group search-form">
						<input type="text" name="fullsearch" class="search-input col-sm-10">
					
						<div class="col-sm-2 margin_zero padding_zero">
							<a href="#" class="btn-standart-grey search-btn-grey">ИСКАТЬ</a>
						</div>
						
						<label>
						    <input class="checkbox" type="checkbox" name="checkbox-test">
						    <span class="checkbox-custom"></span>
						   
						</label>
						<label class="exact-match-lbl">Точное совпадение всех слов</label>
					</form>
				</div>
			<div class="col-sm-12 search-parametrs">

				<h2 class="addparam-head">Допонительные параметры:</h2>
				<p class="fullmapParametrs2 fullmapParametrs2-active">По дате</p>
				<p class="fullmapParametrs2">По рейтингу</p>
				<p class="search-par usial-bill">Средний чек:</p>
				<p class="fullmapParametrs2 fullmapParametrs2-active">До 100 ₴</p>
				<p class="fullmapParametrs2">100–500 ₴</p>
				<p class="fullmapParametrs2">500—1000 ₴</p>
				<p class="fullmapParametrs2">1000–1500 ₴</p>
				<p class="fullmapParametrs2">1500—2000 ₴</p>
				<p class="fullmapParametrs2">От 2000 ₴</p>
 
			</div>

			<form>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Основные категории</label>
					<select class="form-control border_radius_zero search-forms">
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
					</select>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Подкатегории</label>
					<select class="form-control border_radius_zero search-forms">
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
					</select>
				</div>
				<div class="col-sm-4 search-empty-block search-forms-blocks">


				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Район города</label>
					<select class="form-control border_radius_zero search-forms">
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
						<option class="search-options"></option>
					</select>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Станция метро</label>
					<select class="form-control border_radius_zero search-forms">
						<option class="search-options">Все</option>
						<option class="search-options">Харьковская</option>
						<option class="search-options">Позняки</option>
						<option class="search-options">Славутич</option>
					</select>
				</div>
				<div class="col-sm-4 search-forms-blocks">
					<label class="search-par">Укажите улицу</label>
					<input type="text" name="searchstreet" class="form-control border_radius_zero search-forms">
				</div>


				<div class="col-sm-12">
					<div>
						<label>
							    <input class="checkbox" type="checkbox" name="checkbox-test">
							    <span class="checkbox-custom"></span>
							    <span class="fullsch-ckb-sp1">Заведение открыто</span>
						</label>
					</div>
					<div>
						<label>
							    <input class="checkbox1" type="checkbox" name="checkbox-test">
							    <span class="checkbox-custom1"></span>
							    <span class="fullsch-ckb-sp2">Наличие 3D-тура</span>
						</label>
					</div>
				</div>

				<div class="col-sm-12 fullsrchbtn-block">
					<a href="#" class="btn-small-blue fullsearch-btn">НАЧАТЬ ПОИСК</a> 
					<a href="#" class="btn-standart-grey fullsearch-btn-grey">СБРОСИТЬ</a>

				</div>
		

			</form>

				<div class="col-sm-12 bigHeader">
				<p class="ntfnd-big-parag">самое</p>
			</div>


				<a href="#" class="col-sm-4 blackoutPictureLink typeBlock_300x335">
					<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">CITY NEWS</h5>
								<p class="paragraf">Чужой на карте среди своих</p>
							</figcaption>
					</figure>
				</a>

				<a href="#" class="col-sm-4 blackoutPictureLink typeBlock_300x335">
					<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
								<p class="paragraf">Дух не спрятать за немецкими зданиями</p>
							</figcaption>
					</figure>
				</a>

				<a href="#" class="col-sm-4 blackoutPictureLink typeBlock_300x335">
					<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">GASTRO LIFE</h5>
								<p class="paragraf">Хозяева без золотой середины</p>	
							</figcaption>
					</figure>
				</a>

			</div>
		</div>


	</div>

</div>


<?php
	get_footer();
?>