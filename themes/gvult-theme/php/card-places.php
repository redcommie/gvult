
<?php



$arrIDCat = [3,5,6,24,27,7];
$countArrIDCat = count($arrIDCat);

$arrCatImgs = array(
	'3'		=> 'img_cat_3',
	'5'		=> 'img_cat_5',
	'6'		=> 'img_cat_6',
	'24'	=> 'img_cat_24',
	'27'	=> 'img_cat_27',
	'7'		=> 'img_cat_7'

	// '5'		=> get_template_directory_uri() . '/img/group-7.svg',
	// '6'		=> get_template_directory_uri() . '/img/group-4.svg',
	// '24'	=> get_template_directory_uri() . '/img/group-5.svg',
	// '27'	=> get_template_directory_uri() . '/img/group-12.svg',
	// '7'		=> get_template_directory_uri() . '/img/group-6.svg'


	);

for ( $i = 0; $i < $countArrIDCat; $i++  ) :
	$category = ss_get_category_info($arrIDCat[$i]);
?>
	<div class="col-md-4 col-sm-4 col-xs-6 catalog_block">
		<a href="<?php echo $category['link'];?>">
		<div class="catalogElements">
			<p class="catalogHeaders"><?php echo $category['name'];?></p>
			<div class="row block margin_zero padding_zero">
				<p class="catParagrafs">Мест:<?php echo $category['count_posts'];?><br>Отзывов:<?php echo $category['count_comments'];?></p>
					<div class="<?php echo $arrCatImgs[$arrIDCat[$i]]; ?>">
					</div>
				<a href="<?php echo $category['link'];?>" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
			</div>
		</div>
		</a>
	</div>
<?php endfor;
?>

<div class="row margin_zero padding_zero">
	<div class="col-sm-4 catalogIcons">
		<a href="<?php echo ss_get_category_info(31)['link']; ?>">
			<img class="" src="<?php bloginfo('template_url'); ?>/img/home-icon.png" alt="catalog-icons">
			<p>ДЛЯ ДОМА</p>
		</a>
	</div>
	<div class="col-sm-4 catalogIcons">
		<a href="<?php echo ss_get_category_info(4)['link']; ?>">
			<img class="" src="<?php bloginfo('template_url'); ?>/img/kids-icon.png" alt="catalog-icons">
			<p>ДЕТИ</p>
		</a>		
	</div>
	<div class="col-sm-4 catalogIcons">
		<a href="<?php echo ss_get_category_info(36)['link']; ?>">
			<img class="" src="<?php bloginfo('template_url'); ?>/img/sport-icon.png" alt="catalog-icons">
			<p>СПОРТ</p>
		</a>		
	</div>
</div>

