<?php 
	get_header();

//print_var(get_terms('taxonomy=category'));
?>
<div class="container-fluid">
	<div class="row margin_zero padding_zero addPlaceBlock">
		<form id="form_add_places" class="col-sm-12 col-xs-12">
			<div class="col-sm-12 addplace-selectblock">
				
					<h1 class="addPlaceH1">добавить своё заведение</h1>
					<p class="addNewPlaceFont">Для того что бы написать письмо в редакцию, воспользуйтесь нашей формой:</p>

					<div class="form-group addNameForm">
		    				<label class="addNewPlaceFont">Название</label>
		    				<input id="name_place" type="text" class="form-control border_radius_zero" placeholder="" >
		  			</div>
			</div>
			<div class="col-sm-4 newPlaceSelect">
					<label class="addNewPlaceFont">Основные категории</label>
					<input id="main_category" type="text" class="selectCategory input_list" placeholder="Выберите категорию" readonly>
					<span class="select-arrow-down"></span>
					<ul class="list addplace-selectlist">
						<?php 
							$categories = ss_get_parent_categories();
							for ( $i = 0; $i < count($categories);  $i++ )
							{ 
								echo '<li data-value="'.$categories[$i]->name.'" data-id="'.$categories[$i]->term_id.'">'.$categories[$i]->name.'</li>';
							}
						?>
					</ul>
			</div>

			<div class="col-sm-4 newPlaceSelect">
					<label class="addNewPlaceFont">Подкатегории</label>
					<input id="sub_category" type="text" class="selectCategory input_list" placeholder="Выберите категорию" readonly>
					<span class="select-arrow-down"></span>
			</div>
			
			<div class="col-sm-4 newPlaceSelect">
					<label class="addNewPlaceFont">Средний чек</label>
					<input id="input_averagecheck" type="text" class="selectBill input_list" readonly>
					<span class="select-arrow-down"></span>
					<ul class="list addplace-selectlist
					">
						<?php 
							$average_checks = get_average_checks();
							for ( $i = 0; $i < count($average_checks);  $i++ )
							{ 
								echo '<li data-value="'.$average_checks[$i]['name'].'" data-min="'.$average_checks[$i]['min'].'">'.$average_checks[$i]['name'].' &#8372;</li>';
							}
						?>
					</ul>
			</div>
			<div class="col-sm-6 divForAddFoto">
					<label class="addNewPlaceFont">Вы можете сразу добавить несколько фото в галерею</label>
					<input id="photo" type="file" name="foto_download" id="file" class="inputfile "  accept="image/*">

					<label class="addplace-labelfile">Добавить фото<span class="filebtn-icon"></span></label>
					
			</div>
		</form>
	</div>
	<div class="row margin_zero padding_zero addPlaceBlock">
		<div class="col-sm-12 ">
			<div class="col-sm-12 padding_zero">
				<p class="addNewPlaceFont">Описание</p>

				<textarea id="description" class="form-control textareaMargin border_radius_zero" rows="12"></textarea>
			</div>
		</div>
	</div>


	<div class="row moreInfoBlock addPlaceBlock">
		<div class="col-sm-12">
			<p id="btn_additional_options" class="moreInfoButton moreinfo-arrowshow moreinfo-arrowhide">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</p>


			<form id="form_additional_options" class="col-sm-12 detailedInfo">
				<div class="form-group col-sm-6 addplace-morefields">
    				<label class="addNewPlaceFont">Телефон</label>
    				<input id="number_tel" type="tel" class="form-control border_radius_zero" placeholder="+38044XXXXXXX" size>
  				</div>

  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Режим работы</label>
				    <input id="timework" type="text" class="form-control border_radius_zero" placeholder="">
  				</div>
  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Владелец (Сеть)</label>
				    <input id="owner" type="text" class="form-control border_radius_zero" placeholder="">
  				</div>
  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Ссылка на картинку сети</label>
				    <input id="link_photo" type="text" class="form-control border_radius_zero" placeholder="">
  				</div>
  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Страна</label>
				    <input id="country" type="text" class="form-control border_radius_zero " placeholder="Украина" value="Украина" readonly>
  				</div>
  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Город</label>
				    <input id="city" type="text" class="form-control border_radius_zero" placeholder="Киев" value="Киев" readonly>
  				</div>
  				<div class="form-group col-sm-4 addplace-morefields">
				    <label class="addNewPlaceFont">Aдрес</label>
				    <input id="address" type="text" class="form-control border_radius_zero" placeholder="Улица">
  				</div>
  				<div class="form-group col-sm-2 addplace-morefields">
				    <label class="addNewPlaceFont visibility_Hidden">Дом</label>
				    <input id="number_building" type="text" class="form-control border_radius_zero" placeholder="Дом">
  				</div>
  				<div class="form-group col-sm-3 addplace-morefields">
				    <label class="addNewPlaceFont">Координаты</label>
				    <input id="latitude" type="number" class="form-control border_radius_zero"  placeholder="Широта">
  				</div>
  				<div class="form-group col-sm-3 addplace-morefields">
				    <label class="addNewPlaceFont visibility_Hidden">Долгота</label>
				    <input id="longitude" type="number" class="form-control border_radius_zero"  placeholder="Долгота">
  				</div>

				
				<div class="form-group col-sm-6 addplace-morefields morefields-select">
				    <label class=" addNewPlaceFont">Район Киева</label>
				    <input id="input_district" class="form-control border_radius_zero input_list" type="text" readonly>
				    <span class="selectinfo-arrow-down"></span>
					<ul class="list">
						<?php 
							$districts = get_districts_name();
							for ( $i = 0; $i < count($districts);  $i++ )
							{ 
								echo '<li data-value="'.$districts[$i].'">'.$districts[$i].'</li>';
							}
						?>
					</ul>
  				</div>

  				<div class="form-group col-sm-6 addplace-morefields morefields-select">
				    <label class="addNewPlaceFont">Ближайшая станция метро</label>
				    <input id="subway" class="form-control border_radius_zero input_list" type="text" readonly>
				    <span class="selectinfo-arrow-down"></span>
					<ul class="list">
						<?php 
							$subways = get_subway_names();
							for ( $i = 0; $i < count($subways);  $i++ )
							{ 
								echo '<li data-value="'.$subways[$i].'">'.$subways[$i].'</li>';
							}
						?>
					</ul>
  				</div>

  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Сайт</label>
				    <input id="website" type="text" class="form-control border_radius_zero" placeholder="https://myshop.com">
  				</div>

				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Контакты для связи</label>
				    <input id="contacts" type="text" class="form-control border_radius_zero" placeholder="Не отображаются на сайте">
  				</div>

				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Страница во Вконтакте</label>
				    <input id="twitter" type="text" class="form-control border_radius_zero" placeholder="https://vk.com/myshop">
  				</div>

  				<div class="form-group col-sm-6 addplace-morefields">
				    <label class="addNewPlaceFont">Страница на Фейсбук</label>
				    <input id="facebook" type="text" class="form-control border_radius_zero" placeholder="https://facebook.com/myshop">
  				</div>

  				
  				

				
			</form>
		</div>
	</div>

	<div class="col-sm-12 addPlaceBlock addNewsLastButt">

		<a id="btn_add_place" href="#" class="btn-small-blue" data-nonce="<?echo wp_create_nonce('secret_key')?>">ДОБАВИТЬ</a>
		
	</div>

</div>
	
<?php 
	get_footer(); 
?>