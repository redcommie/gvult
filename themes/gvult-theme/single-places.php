<?php 
	get_header();
?>

<div class="container-fluid restFluid">
	
	<div class="rest-backgroundimage" style="background-image: url(<?php echo get_the_post_thumbnail_url();?>);">
		
	</div>

	<div class="row">
		<div class="col-sm-12 firstRestContainer">
			<div class="col-sm-8 restDescription padding_zero">
				<div class="row">
					<div class= "col-sm-12 rest-maininfoblock">
						<div class="openNowBlock">
							<span class="<?php echo check_opening_place($post->ID) ? 'placecard-openIcon' : 'placecard-lockIcon';?>"></span>
                            <p class="openNowPar"><?php echo check_opening_place($post->ID) ? 'открыто сейчас' : 'закрыто сейчас';?></p>
						</div>
						<h1 class="restH1"><?php the_title(); ?></h1>
							<?php echo ss_getPlaceRating($post);
							$userVotedPostsId = get_user_meta(get_current_user_id(), 'placerating_voted_posts'); ?>
							<div class="rating-block" data-user-ratingid="<?php echo get_current_user_id(); ?>" 
							<?php
								if(is_user_logged_in() && isset($userVotedPostsId[0]) && !in_array($post->ID, $userVotedPostsId[0])){
									?> id="rating-block-id" 
							<?php }
							elseif(!is_user_logged_in()){ ?>
							id="rating-block-id-logout" <?php }
							elseif( isset($userVotedPostsId[0]) && in_array($post->ID, $userVotedPostsId[0]) ){
							?>
                              id="rating-block-id-voted"
							<?php
							}
							?> data-post-ratingid="<?php echo $post->ID; ?>">

                             <?php
                                	$placeRating = round(get_post_meta($post->ID, 'rating_average',true), 1);
                                	
                               		for ($i = 0; $i < 5; $i++) {
	                               		if($i*2 <= $placeRating){
	                               			echo '<span class="price-rating-star single-gold-star" data-level="'.$i.'" data-switch="off"></span>';
	                               		}
	                                   	else{
	                                   		echo '<span class="price-rating-star single-grey-star" data-level="'.$i.'" data-switch="off"></span>';
                                   		}
                                	} ?>
                               
                                <span class="rest_rating_count" data-post-poll-count="<?php echo get_post_meta($post->ID, 'rating_poll_count',true); ?>" data-rating-avarege="<?php echo $placeRating; ?>"><?php echo $placeRating; ?></span>
                            </div>
						<div class="col-sm-12 padding_zero kitchenNameBlock">
							<?php
								$categoriesPlace = get_the_category();
								foreach ($categoriesPlace as $key => $value) {
									?>
									<p class="kitchenNamePar"><?php echo $value->name; ?></p>
									<?php
								}
							?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12">
						<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
						<div class="restTextBlock col-sm-12 margin_zero padding_zero">
								<?php
									while( have_posts() ) : the_post();
										the_content();
									endwhile;
								?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 restProperties">
				<div class="col-sm-12 restPropertiesDiv1">
					<div>
						<img src="<?php bloginfo('template_url');?>/img/add-svg-png/phone.png" alt="phone">
						<p class="rest_properties_font"><?php echo get_post_meta(get_the_ID(), 'phone', TRUE);?></p>
					</div>

					<div>
						<img src="<?php bloginfo('template_url'); ?>/img/add-svg-png/web.png" alt="site">
						<p class="rest_properties_font"><?php echo get_post_meta(get_the_ID(), 'website', TRUE);?></p>
						
					</div>
					
					<div>
						<img src="<?php bloginfo('template_url'); ?>/img/add-svg-png/mail.png" alt="email">
						<p class="rest_properties_font"><?php echo get_post_meta(get_the_ID(), 'email', TRUE);?></p>
					</div>
					<a href="<?php echo get_post_meta(get_the_ID(), 'twitter', TRUE);?>"><img src="<?php bloginfo('template_url'); ?>/img/Twitter-icon.png" class="rest-twitter-icon" alt="twitter"></a>
					<a href="<?php echo get_post_meta(get_the_ID(), 'fb', TRUE);?>"><img src="<?php bloginfo('template_url'); ?>/img/facebook1.png" alt="fb"></a>

					<p class="restMiddleBill rest_properties_font"><?php
						if(get_post_meta(get_the_ID(), 'check', TRUE) !=0){
							 echo 'Средний чек ' . get_post_meta(get_the_ID(), 'check', TRUE). '₴';
						}
					?></p>

				</div>
					
				<div class="col-sm-12 restPropertiesDiv2">
					<table class="restWorkTimeTab rest_properties_font">
						<?php
							echo get_working_time_place($post);
						?>
						
					</table>
				</div>

				<div class="col-sm-12 restPropertiesDiv3  margin_zero padding_zero">

					<div class="col-sm-2 margin_zero padding_zero">
						<img class="addresPointerImg" src="<?php bloginfo('template_url'); ?>/img/add-svg-png/place.png" alt="point">
					</div>

					<div class="col-sm-10 margin_zero padding_zero">
					
						<?php echo getThePlaceAdress($post);?>
					</div>

				</div>

				<div class='row'>
					<div id='map' class="map_place col-sm-12" data-postid="<?php echo $post->ID; ?>" data-posttitle="<?php echo $post->post_title;?>">
					</div>
				</div>
				<div class="col-sm-12 restPropertiesDiv5 padding_zero">
					<div class="padding_zero place-properties">
						<?php
							echo getPlaceProrerty($post);
						?>
						
					</div>
				</div>
			</div>
			<div class="col-sm-8 padding_left_zero">
				<!-- <div  class="col-sm-12 restBtnSclBlock">
					<a href="<?php echo get_post_meta(get_the_ID(), 'fb', TRUE);?>" class="btn-small-blue restSocialBtn fbBtnBlock">
				
						<img src="<?php bloginfo('template_url'); ?>/img/f-b-share.png" alt="fb">
						<span>Share</span>
					</a>

					<a href="<?php echo get_post_meta(get_the_ID(), 'twitter', TRUE);?>" class="btn-small-blue restSocialBtn TwBtnBlock">
						<img src="<?php bloginfo('template_url'); ?>/img/fill-1.png" alt="twitter">
						<span>Tweet</span>
					</a>
				</div> -->
				

				<div class="col-sm-6 col-xs-12 restCard1 padding_left_zero">
					<a href="#">
							<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages" alt="adwert">
					</a>
				</div>
				<div class="col-sm-6 col-xs-12 restCard1 padding_left_zero">
					<a href="#">
							<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages" alt="adwert">
					</a>
				</div>

				<div class="col-sm-12 col-xs-12 restParagrafHead-first padding_left_zero">
					<p class="paragraf">Ближайшие мероприятия</p>
					<div></div>
				</div>
					<?php
						$args = array
							(
								'posts_per_page'		=> 2,
								'post_type'				=> array('post'),
								'cat'					=> array(613, 39)
							);

						$query = new WP_Query($args);
						
						$count = 0;
						while ( $query->have_posts() )
						{
							$query->the_post();
												
							if ( $count != 3 )
							{
								echo '<div class="col-sm-6 col-xs-12 blackoutPictureLink typeBlock_300x335">';
								echo getCardSimpleSingle($post, false);
								echo '</div>';
							}
							
							$count++;
						}
						
						wp_reset_postdata();?>

				<div class="col-sm-12 restRewiewBlock padding_zero">
					
						<div class="restParagrafHead">
							<p class="paragraf">Отзывы пользователей</p>
						</div>								
								
						<form>
							<div class="form-group rest_form_group">
								<label for="comment" class="leaveCommentParagraf">Оставить комментарий</label>
								<textarea id = "item-textarea" class="form-control" rows="6"></textarea>
								
							</div>
							<hr class="paragraph_line">
						</form>
						<!--<div class="restRewiewTextAreaDescript">
							<span>Вы можете сразу добавить несколько фото в галерею</span>
						</div>-->

						<div class="row restAddFottoBlock">
							<div class="col-sm-7 col-xs-5 rest_send_comment">
							<a href="" class="btn-small-blue restSendCommBtn" data-postid="<?php echo $post->ID ?>">ОТПРАВИТЬ</a>
						</div>
				
							<!--	<div class="restAddFotoForm col-sm-5 col-xs-7">
									<input type="file" name="foto_download" id="file" class="inputfileRest"/>
									<label class="rest-addfoto-btn">ДОБАВИТЬ ФОТО</label>
								</div> -->
						</div>
					        <hr class="paragraph_line">
							<div class="restPeopleComments" data-postid="<?php echo $post->ID ?>">
						
							<?php
							    $args = array(
									'post__in'	=> $post->ID,
									'post_type'	=> 'places',
									'number'	=> 10,	
									'parent'	=> 0,
									'hierarchical'	=> false
								);
							
							    $comments = get_comments($args);
								get_the_comment_life($comments);
							?>
						

							<!-- <div class="col-sm-12 padding_zero restCommDeleted">
								<span>КОММЕНТАРИЙ УДАЛЕН</span>
							</div>

							<div class="col-sm-12 padding_zero restCommDeleted">
								<span>КОММЕНТАРИЙ УДАЛЕН МОДЕРАТОРОМ</span>
							</div>

							<div class="col-sm-12 col-xs-12 padding_zero restCommDeleted">
								<span>СВЕРНУТЬ ВСЕ КОММЕНТАРИИ</span>
							</div>-->

						</div>
				</div>
				

				<div class="row restSimilarPlaces">	
							<div class="col-sm-12 padding_left_zero">
								<div class="restParagrafHead-sec col-sm-12">
									<p class="paragraf">Похожие заведения рядом</p>
									<div></div>
								</div>

								<?php
									if(get_the_category( $post->ID )[0]->parent != 0){
										$postParent = get_the_category( $post->ID )[0]->parent;
									}
									else{
										$postParent = get_the_category( $post->ID )[0]->cat_ID;
									}
								
									$args = array
												(
													'posts_per_page'	=> 6,
													'post_type'			=> 'places',
													'cat' 				=> $postParent
												);

									$query = new WP_Query($args);
									$count = 0;
									
									while ( $query->have_posts() )
									{
										$query->the_post();
										
										if ( $count != 6 )
										{
											?><div class="col-sm-4 col-xs-6"><?php 
											echo getCardPlace($post);
											?></div><?php
										}


										$count++;
									}
											
						
								wp_reset_postdata();?>
							</div>
						</div>
			</div>
			<div class="col-sm-4 padding_zero rest-adwert-banner">
				<a href="#"><img src="http://lorempixel.com/313/600" class="img-responsive" alt="ardwert"></a>	
			</div>
		</div>

	</div>
<div class="row restAddPlaceBanner-row">
	<div class="col-md-12 col-xs-12 restAddPlaceBanner-block">
		<div class="restAddPlaceBanner col-md-12 col-xs-12">
					<p class="restAddPlaceBannerHead">Добавьте своё заведение</p>
					<p class="restAddPlaceBannerPar">Если вы владелец магазина, ресторана или коворкинг площадки, добавьте свое заведение на «Гвалт».</p>
					<a href="<?php echo get_home_url() ?>/dobavit-zavedenie/" class="btn-standart-border restAddPlaceBannerBtn">ДОБАВИТЬ</a>
		</div>
	</div>
</div>

<div class="row">
			
		<div class="col-sm-12 col-xs-12 secondRestContainer">
			


			<div class="row">
				<div class=" col-sm-8 col-xs-12 padding_zero">
							<div class="restParagrafHead-third col-sm-12 col-xs-12">
										<p class="paragraf">Новые заведения</p>
										<div></div>
							</div>
						
									<?php
									$args = array
										(
											'posts_per_page'	=> 6,
											'post_type'			=> 'places'
										);

										$query = new WP_Query($args);
										$count = 0;
										
										while ( $query->have_posts() )
										{
											$query->the_post();
											
											if ( $count != 6 )
											{
												?><div class="col-sm-4 col-xs-6"><?php 
												echo getCardPlace($post);
												?></div><?php
											}


											$count++;
										}
										
					
										wp_reset_postdata();?>
									
									
				
				</div>

				<div class="col-sm-4 col-xs-12">
					<div class="reviews rest-reviewblock">
						<h4 class="last-reviews-head">Последние отзывы заведений</h4>
						<?php echo ss_getComments(4, 0, 3,'reviewBlocks');?>
						<!-- <div>
						<a href="#" class="btn-standart-border ind-review-btn"  title="">БОЛЬШЕ ОТЗЫВОВ</a>
						</div> -->
					</div>
				</div>
			</div>

		</div>
	</div>

</div>






<?php 
	get_footer();
	
?>
