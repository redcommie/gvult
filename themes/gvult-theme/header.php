<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Здесь пишем все метатеги -->
    <meta charset="utf-8">
    <title><?php wp_title('|',1,'right'); ?> Gvult</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:image:type"         content="image/jpeg" />
    <meta property="og:image:width"        content="1200" />
    <meta property="og:image:height"       content="630" />

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php if ( WORKING_VARIANT ):?>
        <!-- Вход через google -->
        <script src="https://apis.google.com/js/api:client.js"></script>
        <script>
            var googleUser = {};
            var startApp = function()
            {
                gapi.load('auth2', function(){
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init(
                {
                    client_id: '980953484047-skthm9qd4stjgrirg523034p7dchdhcm.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                    // Request scopes in addition to 'profile' and 'email'
                    //scope: 'additional_scope'
                });

                attachSignin(document.getElementById('google_login'));
                });
            };

            function attachSignin(element)
            {
                auth2.attachClickHandler(element, {},
                function(googleUser)
                {
                    var profile = googleUser.getBasicProfile();

                    var request = new XMLHttpRequest()
                    var fd = new FormData()

                    request.open('POST', ajax_request_object.ajaxurl)
                    fd.append('action', 'ajaxdatausers')
                    fd.append('ID', profile.getId())
                    fd.append('firstName', profile.getGivenName())
                    fd.append('lastName', profile.getFamilyName())
                    fd.append('email', profile.getEmail())
                    fd.append('image', profile.getImageUrl())
                    fd.append('nonce', google_login.dataset.nonce)


                    request.send(fd)

                    request.onreadystatechange = function()
                    {
                        if ( this.readyState != 4 )
                            return

                        if ( this.status == 200 )
                        {
                            //var answer = JSON.parse(this.responseText)
                            //var answer = this.responseText

                            location.reload()
                        }
                    }
                },
                function(error)
                {
                    alert(JSON.stringify(error, undefined, 2));
                });
            }
        </script>
    <?php endif;?>
    <?php if( is_singular() ) wp_enqueue_script('comment-reply'); ?>
    <!-- Вход через google -->
    <?php
    wp_head(); // Эта функция обязательно вызывается в Хеде
    $menu_items = wp_get_nav_menu_items('header_menu'); // = top-menu - это название, которое пользователь указывает в настройках меню в админке
    $page_id = get_the_ID();
    $page_for_posts = get_option('page_for_posts');
    $is_home = is_home(); // Берем ID страницы, и проверяем, является ли текущая страница главной или страницей для записей

    // Получаем пользователя
        $isUser = false;
        if ( is_user_logged_in() )
        {
            $userAvatarData = get_avatar_data(wp_get_current_user());
            $userMeta = get_user_meta(get_current_user_id());
            $userData = wp_get_current_user();   
            
            $userAvatarUrl = $userAvatarData['found_avatar'] ? $userAvatarData['url'] : $userMeta['avatar_small'][0];
            $userLoginName = array_key_exists('first_name', $userMeta) ? $userMeta['first_name'][0] : $userData->user_login;
            $userName = $userData->display_name;
            $userEmail = array_key_exists('email', $userMeta) ? $userMeta['email'][0] : $userData->user_email;

            $userEnteredFrom = array_key_exists('enter_from', $userMeta) ? $userMeta['enter_from'][0] : '';

            $isUser = true;
        }

    // ф-ция выводит иконки соц сетей для входа
    function ss_getSocIcon()
    {
        echo '<div class="reg-form-socialBlock">
                <div class="red-form-socialIcons"><a href="'.get_socauth_link('tw').'" class="soc-auth-link"><img src="'.get_template_directory_uri().'/img/twitterIcon.png" alt=""></a></div>
                <div class="red-form-socialIcons"><a href="'.get_socauth_link('fb').'" class="soc-auth-link"><img src="'.get_template_directory_uri().'/img/facebookIcon.png" alt=""></a></div>
                <div class="red-form-socialIcons"><a id="google_login" href="#" data-nonce="'.wp_create_nonce('check-google').'"><img src="'.get_template_directory_uri().'/img/new-google-favicon.png" alt=""></a></div>
            </div>';
    }
    ?>
</head>
<body>
 
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '117897172124094',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>

<header class="header_phone row">

        <div class="col-xs-2 head_phone_menu"></div>
        <div class="col-xs-8 header_phone_logo">
            <a href="<?php echo home_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/img/png-svg-gvult/Logo.svg" alt="logo"></a>
        </div>
        <div id="header_phone_search" class="col-xs-2 header_phone_search">
            <span id="search_mobile" class="header_mobile_loupe"></span>
        </div>
        <div class="col-xs-12 container_header_search_mobile padding_zero">
            <div class="col-xs-2 header_phone_search">
                <span id="search_mobile" class="header_mobile_loupe"></span>
            </div>
            <form id="header_search_mobile" class="col-xs-8 header_search_mobile padding_zero">
                <label class="headsearch-label">
                    <input type="text" id="auxiliary_search">
                    <select id="select-tools-mobile"></select>
                </label>
                <!-- <a id="btn_search" href="#" class="btn-small-blue btn-loupe"><img src="<?php echo get_template_directory_uri(); ?>/img/magnifying-glass-white.png" class="btn-loupe-icon" alt="seach_button"></a> -->
            </form>
            <div class="col-xs-2">
                <span class="close_search_mobile"></span>
            </div>
       </div>
</header>

<div class="main_phone_menu">
<span class="close-mobile-menu"></span>
    <nav>

        <ul class="phone_navigation">
            <!-- <li>city guide</li>
            <li>city news</li>
            <li>gastro life</li>
            <li>culture</li>
            <li>FASHION&BEAUTY</li> -->
             <?php // Пример реализации вывода меню, сохраненного в админке. Текущему пункту меню назначается класс selected, также проверяется target=blank
                            foreach ($menu_items as $item) {
                                if ($item->object_id == $page_id || ($is_home && $item->object_id == $page_for_posts) ) $selected = ' class="selected"';
                                else $selected = '';
                                if ($item->target == '_blank') $target = ' target="_blank"'; else $target = '';
                                ?><li class="mobile-nav-list"><a href="<?php echo $item->url; ?>" class="headerNav-link-mobile" <?php echo $target.$selected; ?>><?php echo $item->title; ?></a></li><?php
                            }


                     ?>
     

        </ul>
        </br>
        <ul class="submenu_phone">
            <li>Контакты</li>
            <li>Реклама</li>
            <li>О нас</li>
            <li>Вакансии</li>
            <li>Соглашение</li>
            <li>Правила общения</li>
            <li>Карта сайта</li>
            </br>
            <li>Добавить ваше заведение</li>
            <li>Добавить отзыв</li>
        </ul>
    </nav>

<div class="login_phone_block">
    <?php
    if(is_user_logged_in()){
    ?>
    <div class="avatar_phone_block">
        <img class="img-responsive" src="<?php echo $isUser ? $userAvatarUrl : get_template_directory_uri().'/img/login-icon.png'; ?>" alt="login_icon">
        <?php if ($isUser) :?>
            <span id="nicknameUser"><?php echo empty($userLoginName) ? $userName : $userLoginName; ?></span>
        <?php endif;?>
    </div>
    <?php
    }
    else{ ?>
        <img class="login_phone_icon img-responsive" src="<?php echo $isUser ? $userAvatarUrl : get_template_directory_uri().'/img/login-icon.png'; ?>" alt="login_icon">
     <?php } ?>
    <span class="phone_login_block_arrow"></span>
</div>
 


</div>


<header class="header">
    <div class="row margin_zero padding_zero">
        <div class="col-xs-12 col-sm-1 headerIconBlock">
            <a href="<?php echo home_url();?>"> <img src="<?php echo get_template_directory_uri(); ?>/img/png-svg-gvult/Logo.svg"  class="headerLogo" alt="logo"></a>
        </div>
        <div class="col-xs-12 col-sm-10 wr_header_menu_and_search">    
            <nav id='header_menu' class="headerNav">
                <a id='search' href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/magnifying-glass.svg"  class="headerMagnifLogo img-responsive" alt="search-logo"></a>
                <ul class="headerNav-menu">
                     <?php // Пример реализации вывода меню, сохраненного в админке. Текущему пункту меню назначается класс selected, также проверяется target=blank
                            foreach ($menu_items as $item) {
                                if ($item->object_id == $page_id || ($is_home && $item->object_id == $page_for_posts) ) $selected = ' class="selected"';
                                else $selected = '';
                                if ($item->target == '_blank') $target = ' target="_blank"'; else $target = '';
                                ?><li class="headerNav-list"><a href="<?php echo $item->url; ?>" class="headerNav-link" <?php echo $target.$selected; ?>><?php echo $item->title; ?></a></li><?php
                            }
                     ?>
                    <li class="headerNav-list header-submenu">
                        <a href="#">  
                            <span class="header-submenu-point"></span>
                            <span class="header-submenu-point"></span>
                            <span class="header-submenu-point"></span>
                        </a>
                        <ul class="submenu-list">
                            <li><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-icon.png" alt="arrow"></li>
                            <li><a href="full_map.html">Контакты</a></li>
                            <li><a href="feedback.html">Реклама</a></li>
                            <li><a href="city_guide.html">О нас</a></li>
                            <li><a href="category_places_filter.html">Вакансии</a></li>
                            <li><a href="category_desc.html">Соглашение</a></li>
                            <li><a href="add_place.html">Правила общения</a></li>
                            <li><a href="404_page.html">Карта сайта</a></li>
                            <li><a href="checkboxes.html">Добавить ваше сообщение</a></li>
                            <li><a href="#">Добавить отзыв</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <form id="header_search" class="header-search">
                <label class="headsearch-label">
                    <input type="text" id="auxiliary_search">
                    <select id="select-tools" placeholder=""></select>
                    <!-- <input type="text" name="headsearch"  class="form-group "> -->
                    <a href="<?php echo get_home_url() ?>/?s&add" class="advanced-search-href">РАСШИРЕННЫЙ ПОИСК</a>
                   <!--  <div class="headsearch-res">
                    </div> -->
                </label>
                <a id="btn_search" href="#" class="btn-small-blue btn-loupe"><img src="<?php echo get_template_directory_uri(); ?>/img/magnifying-glass-white.png" class="btn-loupe-icon" alt="seach_button"></a>
            </form>
        </div>
    <div class="col-xs-12 col-sm-1 header-login-block">
        <a id="btn_login_header" href="" class="<?php echo $isUser ? 'logedUserIcon' : '';?>">
            <img class="loginIcon img-responsive" src="<?php echo $isUser ? $userAvatarUrl : get_template_directory_uri().'/img/login-icon.png'; ?>" alt="login_icon">
            <?php if ($isUser) :?>
                <span id="nicknameUser"><?php echo empty($userLoginName) ? $userName : $userLoginName; ?></span>
            <?php endif;?>
        </a>

        <div id="reg-form" class="reg-form user_menu">
            <form id="form_register" class="form-group" name="formSignIn" method="post">
                <label class="reg-form-label">Регистрация</label>
                <span class="header-close-arrow">
                    
                </span>
                <input type="text" name="login_reg" class="form-control border_radius_zero reg-form-margin-form" placeholder="Логин" data-name="login">
                <input type="password" name="pass_reg" class="form-control border_radius_zero reg-form-margin-form" placeholder="Пароль" data-name="password">
                <!-- <p class="reg-form-wrong-pass">Вы не ввели пароль</p> -->
                <input type="password" name="pass_reg_repiat" class="form-control border_radius_zero reg-form-margin-form" placeholder="Пароль ещё раз" data-name="passRepiat">
                <input type="email" name="email_reg" class="form-control border_radius_zero reg-form-margin-form" placeholder="Почта" data-name="email">
                <input type="hidden" value="<?php echo wp_create_nonce('secure_code_reg');?>" data-name="security">
            </form>

            <p class="reg-form-withparag">ЗАРЕГИСТРИРОВАТЬСЯ С ПОМОЩЬЮ</p>
            <?php ss_getSocIcon();?>
            <div class="reg-form-btn">
                <a id="btn_reg_user" href="#" class="btn-standart-blue">ЗАРЕГИСТРИРОВАТЬСЯ</a>

            </div>

            <div class="reg-form-lastDiv">
                <a id="btnShowLoginForm" href="#" class="reg-form-bluePar1">Есть аккаунт? Войдите</a>
            </div>
        </div>

        <div id="login-form" class="login-form user_menu">
            <form id="singin_form" class="form-group">
                <label class="reg-form-label">Войти</label>
                <span class="header-close-arrow">
                   
                </span>
                <input type="text" name='login_singin' class="form-control border_radius_zero reg-form-margin-form registrFormsHovers" placeholder="Логин">
                <input type="password" name='pass_singin' class="form-control border_radius_zero reg-form-margin-form registrFormsHovers" placeholder="Пароль">
                <input type="hidden" value="<?php echo wp_create_nonce('security_recovery');?>" data-name="security">
            </form>
            <div>
                <a id="btn_sign_in" href="#" class="btn-small-blue login-form-btn">ВОЙТИ</a>
                <a id="link_pass_recovery" href="#" class="reg-form-bluePar">Вспомнить пароль</a>
            </div>
            <p class="reg-form-withparag">ВОЙТИ С ПОМОЩЬЮ</p>
            <?php ss_getSocIcon();?>
            <div class="reg-form-lastDiv">
                    <a id="btnShowRegForm" href="#" class="reg-form-bluePar2">Нет аккаунта? Регистрация</a>
            </div>
        </div>
        <div id="profile-form" class="profile-form user_menu">
            <div class="avatar-icon-facebook <?php echo empty($userEnteredFrom) ? '' : ($userEnteredFrom == 'facebook' ? 'fb-onAvatar' : ($userEnteredFrom == 'google' ? 'gl-onAvatar' : 'tw-onAvatar'))?>">
                <img src="<?php echo $userAvatarUrl?>" alt="<?php echo $userName;?>" class="headerAvatar">
            </div>
            <div class="profile-form-info">
                <a href="<?php echo home_url('/profile/')?>"> <span><?php echo $userName;?></span></a>
                <span class="header-close-arrow"></span>
                <p><?php echo $userEmail;?></p>
            </div>
                <a href="<?php echo home_url().'/soobshheniya'; ?>" class="profile-form-links">Сообщения</a>
                <a href="#" class="profile-form-links">Новости</a>
                <a href="<?php echo wp_logout_url( array_key_exists( 'HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : home_url() ); ?>" class="profile-form-quit">Выйти</a>
        </div>
      
        </div>
    </div>
</header>
