///////////////  Страница add-place.js ////////////////////////
jQuery( document ).ready(function($)
{
	console.log( "File 'add_place.js' is started!" );
	var speedShow = 400

	// Слушаем поля ввода
		$('.input_list').on('click', function(event)
		{
			var container = $(this).parent()
			
			$("ul.active").removeClass('active').slideUp(speedShow)
			container.children('ul').addClass('active').slideDown(speedShow)

			container.find('li').on('click', function(event)
			{
				container.children('.input_list').attr(
				{
					value: this.innerHTML,
					'data-value':  this.dataset.value,
					'data-id': this.dataset.id ? this.dataset.id : '',
					'data-min': this.dataset.min ? this.dataset.min : ''
				})
			
				$("ul.active").removeClass('active').slideUp(speedShow)

				if ( container.find(main_category).length )
					get_sub_category(this)

				$(this).off('click')

				event.preventDefault();
			})


			$('body').on('click', closeInputField)

			event.stopPropagation()
		})

	// Показать дополнительные опции
		$(btn_additional_options).on('click', function()
		{
			console.log($(form_additional_options).hasClass('active'))
			$(form_additional_options).hasClass('active') ?
			$(form_additional_options).removeClass('active').slideUp(300) :
			$(form_additional_options).addClass('active').slideDown(500)
		})


	// Отправляем данные введенные пользователем на сервер
		$(photo).on('change', addPhoto)

		addPhoto.photos = {}
		function addPhoto(event)
		{
			console.log('Работает функция "addPhoto"!')
			console.log(this)
			if ( $('.del_photo') )
				$('.del_photo').off('click')

			if ( !$('#container_photo').length )
				$(this).parent().append(
					'<table id="container_photo" class="container_photo">' +
						'<tr>' +
							'<td>' + this.files[0].name + '</td>' +
							'<td class="del_photo" data-id="' + 
												this.files[0].lastModified + '">X</td>' +
					'</tr></table>')
			else if ( $('.del_photo').attr('data-id') != this.files[0].lastModified )
				$(container_photo).append(
					'<tr>' +
						'<td>' + this.files[0].name + '</td>' +
						'<td data-id="' + this.files[0].lastModified +
															 '"class="del_photo">X</td>' +
					'</tr>')
			
			addPhoto['photos'][this.files[0].lastModified] = this.files[0]
			this.value = ''

			$('.del_photo').on('click', function()
			{
				if ( $('.del_photo').length == 1 )
					$('#container_photo').remove()
				else
					$(this).parent().remove()

				delete addPhoto.photos[$(this).attr('data-id')]
				console.log(this)
				console.log(addPhoto.photos)
			})

			// console.log(this.files)
			console.log(addPhoto.photos)
		}

		$('.del_photo').on(function()
		{
			console.log(this)
		})

		$(btn_add_place).on('click', function()
		{
			cancel_error_message()

			if (check_data())
				send_data_new_place(event)

			event.preventDefault();
		})

		function send_data_new_place(event)
		{
			var xhrNewPlace = new XMLHttpRequest()
			var fd = new FormData()

			xhrNewPlace.open('POST', ajax_request_object.ajaxurl)
			fd.append('action', 'ajaxnewplace')
			fd.append('title', name_place.value)
			fd.append('main_category', main_category.dataset.id)
			fd.append('sub_category', sub_category.dataset.id)
			fd.append('averagecheck', input_averagecheck.dataset.value)
			fd.append('check_min', input_averagecheck.dataset.min)
			
			var count = 0
			for ( var id in addPhoto.photos )
				fd.append('img_file_' + count++, addPhoto.photos[id])
			
			fd.append('description', description.value)
			fd.append('number_tel', number_tel.value)
			fd.append('time_work', timework.value)
			fd.append('owner', owner.value)
			//fd.append('link_photo', link_photo.value)
			fd.append('country', country.value)
			fd.append('city', city.value)
			fd.append('address', address.value)
			fd.append('number_building', number_building.value)
			fd.append('latitude', latitude.value)
			fd.append('longitude', longitude.value)
			fd.append('district', input_district.value)
			fd.append('subway', subway.value)
			fd.append('website', website.value)
			fd.append('contacts', contacts.value)
			fd.append('twitter', twitter.value)
			fd.append('facebook', facebook.value)
			fd.append('security', btn_add_place.dataset.nonce)
			fd.append('curUrl', location.href)

			if (xhrNewPlace.readyState)
			{
			    xhrNewPlace.send(fd)
				xhrNewPlace.onreadystatechange = function()
				{
					if (this.readyState != 4) 
						return
				
			        if ( this.status == 200 )
			        {
			        	var responseText = JSON.parse(this.responseText)
			        	//var responseText = this.responseText
			        	//console.log(responseText)
			        	location = responseText
					}
				}
			}			
		}

		function check_data()
		{
			var title = check_title()
			var mainCategory = check_main_category()
			var subCategory = check_sub_category()
			var averageBill = check_average_bill()
			var photo = check_photo()
			var description = check_desc()
			var number_phone = check_number_phone()
			
			if ( title && mainCategory && subCategory && averageBill && photo
						&& description && number_phone )
				return true

			document.body.scrollTop = 0
			return false
		}

		function check_title()
		{
			var regExpText = /^[а-яА-ЯёЁa-zA-Z0-9 ]+$/

			if ( name_place.value == '' || !regExpText.test(name_place.value) ) 
			{
				$(name_place).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Введите название заведения!</p>')
				$(name_place).val('')
				
				return false
			}

			return true
		}

		function check_main_category()
		{
			if ( main_category.value == '') 
			{
				$(main_category).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Выберите категорию!</p>')
				
				return false
			}

			return true
		}

		function check_sub_category()
		{
			if ( sub_category.value == '') 
			{
				$(sub_category).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Выберите подкатегорию!</p>')
				
				return false
			}

			return true
		}

		function check_average_bill()
		{
			if ( sub_category.value == '') 
			{
				$(input_averagecheck).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Укажите средний чек!</p>')
				
				return false
			}

			return true
		}

		function check_photo()
		{
			if ( !$('#container_photo').length ) 
			{
				$(photo).next().addClass('error_input').after('<p class="error_message_auth" style="position: static;">Добавте минимум одну фотографию!</p>')
				
				return false
			}

			return true
		}

		function check_desc()
		{
			if ( description.value == '') 
			{
				$(description).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Расскажите о заведении!</p>')
				
				return false
			}

			return true
		}

		function check_number_phone()
		{
			var regExpText = /^(\+)?[(\-)0-9 ]{1,20}$/
				
			if ( $(number_tel).attr('value') == '' )
				return true

			if ( !regExpText.test(number_tel.value) ) 
			{
				$(number_tel).addClass('error_input').after('<p class="error_message_auth" style="position: static;">Введите номер телефона в формате "+380441234567"</p>')
				$(number_tel).val('')
				
				return false
			}

			return true
		}

		function cancel_error_message()
		{
			if ($('.error_message_auth'))
			{
				$('.error_input').removeClass('error_input')
				$('.error_message_auth').remove()
			}
		}	
});

