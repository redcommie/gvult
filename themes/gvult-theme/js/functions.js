console.log( "File 'functions.js' is started!" );

function closeInputField()
{

	//jQuery("ul.active").parent().removeClass('active')
	jQuery("ul.active")
		.removeClass('active')
		.slideUp(400,function()
			{
				jQuery(this).parent().removeClass('active')
			})
	
	jQuery('body').off('click', closeInputField)
}

// Функция запрашивает у сервера подкатегории выбранной категории
	function get_sub_category($element, pageSearch)
	{
		var xhrSubCategory = new XMLHttpRequest()
					
		var fd = new FormData()

		xhrSubCategory.open('POST', ajax_request_object.ajaxurl)
		fd.append('action', 'ajaxsubcategory')
		fd.append('id', $element.dataset.id)
		// fd.append('security', dataForm.fields.$security.value)

		if (xhrSubCategory.readyState)
		{
		    xhrSubCategory.send(fd)

        	xhrSubCategory.onreadystatechange = function()
			{
				if (this.readyState != 4) 
					return
			
		        if ( this.status == 200 )
		        {
		        	var subCategories = JSON.parse(this.responseText)
		        	//var responseText = this.responseText
		        	console.log(subCategories)

		        	loadSubCattegories(subCategories, pageSearch)
				}
			}
		}
	}

	// Функция загружает подкатегории в списко подкатегорий
		function loadSubCattegories(categories, pageSearch)
		{
			if ( pageSearch === undefined )
				pageSearch = false

			if ( jQuery(sub_category).parent().find(".list").length )
			{
				jQuery(sub_category).parent().find(".list").remove()
				jQuery(sub_category).attr('value', '')
			}
			jQuery(sub_category)
				.after(
					'<ul class="list' + 
					(pageSearch ? ' filter_field' : '') +
					'"></ul>')

			for ( var i in categories )
				jQuery(sub_category).next().append(
					'<li data-id="' + categories[i]['id'] +
								'" data-value="' + categories[i]['name'] + '">' 
								+ categories[i]['name'] + 
					'</li>')			
		}


// Функция отправляет ajax-запрос
// получает html-карточки постов
function sendDataMorePosts( options )
{
	if ( options.maxcountposts === undefined || 
		options.offset === undefined )
	{
		console.log( 'Ошибка!\nНе указано максимальное количество постов или отступ.' )
		return
	}

	var ajaxDataAddPosts = new XMLHttpRequest()
				
	var fd = new FormData()
	
	ajaxDataAddPosts.open('POST', ajax_request_object.ajaxurl)
	fd.append('action', 'ajaxuploaddataposts')
	fd.append('maxcountposts', options.maxcountposts)
	fd.append('offset', options.offset)
	fd.append('posttype', options.posttype)
	fd.append('cardtype', options.type)
	fd.append('countposts', options.countposts)

	if ( options.search == 'true' )
		fd.append('search', location.search)
	
	if (ajaxDataAddPosts.readyState)
	{
    	ajaxDataAddPosts.send(fd)

		ajaxDataAddPosts.onreadystatechange = function()
		{
			if (this.readyState != 4) 
				return
			
			if ( this.status == 200 )
			{
				var respData = JSON.parse(this.responseText)
				//var respData = this.responseText
				//console.log(respData)
				jQuery('#wr_' + options.id)
					.before(respData.content.join(''))
					
				if ( respData.showButton )
				{
					jQuery('#' + options.id).attr('data-offset', respData.offset)

					if ( jQuery('#' + options.id).attr('data-maxcountposts') == 0 )
						jQuery('#' + options.id)
							.attr('data-maxcountposts', respData.maxcountposts)
				}
				else		
					jQuery('#' + options.id).remove();
			}
		}
	}
}

// Функция отправляет ajax-запрос c данными о комментариях
// получает html-код комментариев
function sendDataMoreComments( options )
{
	if ( options.maxcountcomments === undefined || 
		options.offset === undefined )
	{
		console.log( 'Ошибка!\nНе указано максимальное количество постов или отступ.' )
		return
	}

	var ajaxDataAddPosts = new XMLHttpRequest()
				
	var fd = new FormData()
	
	ajaxDataAddPosts.open('POST', ajax_request_object.ajaxurl)
	fd.append('action', 'ajaxuploaddatacomments')
	fd.append('maxcountcomments', options.maxcountcomments)
	fd.append('offset', options.offset)
	fd.append('countcomments', options.countcomments)
	fd.append('catid', options.catid)
	fd.append('classes', options.classes)

	if ( options.search == 'true' )
		fd.append('search', location.search)
	
	if (ajaxDataAddPosts.readyState)
	{
    	ajaxDataAddPosts.send(fd)

		ajaxDataAddPosts.onreadystatechange = function()
		{
			if (this.readyState != 4) 
				return
			
			if ( this.status == 200 )
			{
				var respData = JSON.parse(this.responseText)
				//var respData = this.responseText
				//console.log(respData)
				jQuery('#wr_' + options.id)
					.before(respData.content)
					
				if ( respData.showButton )
				{
					jQuery('#' + options.id).attr('data-offset', respData.offset)

					if ( jQuery('#' + options.id).attr('data-maxcountcomments') == 0 )
						jQuery('#' + options.id)
							.attr('data-maxcountcomments', respData.maxcountposts)
				}
				else		
					jQuery('#' + options.id).remove();
			}
		}
	}
}

// Ф-ция переключает переключатель со списка на карту и обратно
	function switch_list_map(element)
	{
		element.style.cursor = 'wait'
		var path = location.origin + location.pathname
		
		if ( jQuery(element).attr('data-switch') == 'list')
		{
			if (location.search)
				location = path + '&map'
			else if ( location.pathname == '/category/city-guide/')
				location = path + '?maps'
			else			
				location = path + '?map'
		
			jQuery(element).attr('data-switch', 'map')
			element.firstElementChild.style.marginLeft = '17px';
			element.firstElementChild.style.transition = 'margin-left 0.4s linear';
		}
		else
		{
			location = path
			jQuery(element).attr('data-switch', 'list') 
			element.firstElementChild.style.marginLeft = '1px';
			element.firstElementChild.style.transition = 'margin-left 0.4s linear';
		}
	}