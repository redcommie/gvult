///////////////  Страница category.php ////////////////////////
jQuery( document ).ready(function($)
{
	console.log( "File 'main_places_cat.js' is started!" );
	
	// Объявляем переменные, функции
		var speedShow = 400
		var link = location.href
		var link_path = location.origin + location.pathname
		var link_search = location.search

		function closeInputField()
		{

			$("ul.active").parent().children('span').removeClass('active')
			$("ul.active").removeClass('active').slideUp(speedShow, function(){ $(this).parent().css('z-index', '')})

			$('body').off('click', closeInputField)
		}

		// Функция перезаписывает строку get-запроса поиска 
		function overwrite_str_search( str_search, str_overwrite /*идентификатор строки перезаписи*/, value_overwrite )
		{
			var arrGet = link_search.slice(1).split('&')
			var arrGetTemp = []
			var count = 0

			for ( var i = 0; i < arrGet.length; i++ )
			{
				if ( arrGet[i].indexOf( 'address' ) != -1 && str_overwrite != 'address')
					continue

				if ( arrGet[i].indexOf(str_overwrite) == -1 )
					arrGetTemp[count++] = arrGet[i]
			}

			arrGetTemp[arrGetTemp.length] = str_overwrite + '=' + value_overwrite
			str_search = arrGetTemp.join('&')
			return location.origin +
					 location.pathname + 
					 '?' + 
					 str_search
		}

		// Функция переводит по ссылке с корректированной строкой поиска
		function location_new_search( str_search/*Текущая поисковая строка*/, nameAttr/*Имя ключа запроса*/, value /*Значение ключа*/)
		{
			var indexStart = 0
			var indexEnd = 0;

			if ( !str_search )
				location = link_path + '?' + nameAttr + '=' + value
			else if ( str_search.indexOf(nameAttr) != -1 )
				location = overwrite_str_search( str_search, nameAttr, value )
			else
			{
				if ( str_search.indexOf('address')  != -1 )
				{
					indexStart = str_search.indexOf('&address') != -1 
								? str_search.indexOf('&address')
								: str_search.indexOf('address')
					indexEnd = str_search.indexOf('&', indexStart+1) != -1 
								? str_search.indexOf('&', indexStart+1)
								: str_search.length

					search = str_search.substr(0, indexStart ) 
								+ str_search
									.substr
									(
										indexStart == 1 
											? indexEnd + 1
											: indexEnd,
										str_search.length
									)
					str_search = search
				}

				location = link_path + str_search + '&' + nameAttr + '=' + value
			}
		}

		// Выбор адреса заведения
		function get_address_places()
		{
			var xhr = new XMLHttpRequest()
			var fd = new FormData()

			xhr.open('POST', ajax_request_object.ajaxurl)
			fd.append('action', 'ajaxpostsaddress')

			if (xhr.readyState)
			{
				xhr.send(fd)

				xhr.onreadystatechange = function()
				{
					if (this.readyState != 4) 
						return
				
			        if ( this.status == 200 )
			        {
			        	
			        	//var posts = this.responseText
			        	var addresses = JSON.parse(this.responseText)
			       		 	//console.log(addresses)
			       		
						$('#select-address').parent().children('span').remove()
			        	$('#select-address').selectize({
							valueField: 'meta_value',
							labelField: 'meta_value',
							searchField: 'meta_value',
							options: addresses,
							create: true,
							addPrecedence: true
						});

						$("#select-address").change(function()
						{
							location_new_search( link_search, 'address', this.value)
						})

						var $fieldAddress = document.getElementById('select-address-selectized')
						$fieldAddress.style.width = '100%' 

						var arrAddress = location.search.split('&')
						var valueAddress = ''

						for ( var i = 0; i < arrAddress.length; i++ )
						{
							if ( arrAddress[i].indexOf('address') != -1 )
								valueAddress = decodeURI(arrAddress[i].split('=')[1])
						}

						$fieldAddress.value = valueAddress 
			        }
			    }
			}
		}

	if ( location.search )
	{
		get_address_places()
	}

	// Активируем переключатель список-карта
		$(switcher_list_map).on('click', function()
		{
			switch_list_map(this)
		})

	// Показазываем дополнительные опции сортировки
		$(btn_additional_sort).on('click', function(event)
		{
			$(this).animate(
			{
				opacity: 0
			}, 
			300,
			function()
			{
				$(this).css('display', 'none')
				$(form_additional_sort).slideDown(700).addClass('active')
			})
			
			get_address_places()
		})

	// Слушаем поля ввода
		$('.input_list').on('click', function(event)
		{
			if ( $("ul.active").length )
			{
				closeInputField()
				return
			}

			var container = $(this).parent()
			
			$("ul.active").removeClass('active').slideUp(speedShow)
			container.css('z-index', '4').children('ul').addClass('active').slideDown(speedShow)
			container.children('span').addClass('active')


			container.find('li').on('click', function(event)
			{
				container.children('.input_list').attr(
				{
					value: this.innerHTML,
					'data-value':  this.dataset.value,
					'data-id': this.dataset.id ? this.dataset.id : '',
					'data-min': this.dataset.min ? this.dataset.min : ''
				})
				
				
				location_new_search( link_search, this.dataset.type, this.dataset.type == 'subcat' ? this.dataset.id : this.dataset.value )
				closeInputField();

				$(this).off('click')

				event.preventDefault();
			})


			$('body').on('click', closeInputField)

			event.stopPropagation()
		})

	// Реакция на нажатие кнопки "Заведение открыто"
		$(checkbox_place_open).on('change', function()
		{
			if ( this.checked )
				location_new_search( link_search, this.dataset.type, 'true' )
			else
				location_new_search( link_search, this.dataset.type, 'false' )
		})

	// активируем кнопку "Еще Заведения"
		$('#btnMorePlaces').on('click', function(event)
		{ console.log(this)
			sendDataMorePosts(
			{
				id: this.id,                
				maxcountposts: this.dataset.maxcountposts, 
				offset: this.dataset.offset, 
				posttype: this.dataset.posttype,
				search: this.dataset.search,
				type: this.dataset.type,
				countposts: this.dataset.countposts
			})

			event.preventDefault();
		})

	// активируем кнопку "Еще комментарии"
		$('#btnMoreComments').on('click', function(event)
		{
			sendDataMoreComments(
			{
				id: this.id,                
				maxcountcomments: this.dataset.maxcountcomments, 
				offset: this.dataset.offset, 
				countcomments: this.dataset.countcomments,
				catid: this.dataset.catid,
				classes: this.dataset.classes
			})

			event.preventDefault();
		})
});


