<?php 
	get_header();
?>
<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 feedbackMainContainer">
				<h1 class="addPlaceH1">обратная связь</h1>
				<p class="addNewPlaceFont">Для того что бы написать письмо в редакцию, воспользуйтесь нашей формой:</p>
				<form class="col-sm-12 feedbackForms">
					<div class="col-sm-6 feedbackFormBlock">
						<label class="addNewPlaceFont">Имя</label>
						<input type="text" class="form-control border_radius_zero" placeholder="">
					</div>
					<div class="col-sm-6 feedbackFormBlock">
						<label class="addNewPlaceFont">Фамилия</label>
						<input type="text" class="form-control border_radius_zero" placeholder="">
					</div>
		
					<div class="col-sm-6 feedbackFormBlock">
						<label class="addNewPlaceFont">Тема</label>
						<input type="text" class="form-control border_radius_zero" placeholder="">
					</div>	

					<div class="col-sm-6 feedbackFormBlock">	
						<label class="addNewPlaceFont">Почта</label>
						<input type="text" class="form-control border_radius_zero" placeholder="">
					</div>
	
					<div class="col-sm-12 feedbackTextarea">
						<p class="addNewPlaceFont">Ваше сообщение</p>
						<!-- <textarea class="form-control textareaMargin border_radius_zero" rows="12"></textarea> -->
							<?php

							wp_editor('', 'editor_id', array(
									'wpautop'       => 1,
									'media_buttons' => 0,
									'textarea_name' => '', //нужно указывать!
									'textarea_rows' => 10,
									'tabindex'      => null,
									'editor_css'    => '',
									'editor_class'  => '',
									'teeny'         => 0,
									'dfw'           => 0,
									'tinymce'       => 1,
									'quicktags'     => 0,
									'drag_drop_upload' => false
									) );


								?>
					</div>
					<div class="col-sm-12 feedbackButton">
						<a href="#" class="btn-standart-border">СВЯЗАТЬСЯ С НАМИ</a>
					</div>
				</form>
			</div>
		</div>


		<div class="row bigHeader">
		<div>
			<p class="bigHeaderParagraf">самое</p>
		</div>	
		</div>

		<div class="row">
		<div class="col-sm-12 feedbackCardsContainer">
			<?php
			$args = array
				(
					'posts_per_page'	=> 3,
					'offset'			=> 20,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);
				$count = 0;
				
				while ( $query->have_posts() )
				{
					$query->the_post();
					
					if ( $count != 3 )
					{
						echo '<div class="col-sm-4 blackoutPictureLink typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}
			
					$count++;
				}
				
				wp_reset_postdata();?>
		</div>

	</div>


</div>
	
<?php 
	get_footer(); 
?>
