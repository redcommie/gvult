<div id="myCarousel4" class="carousel slide newsFirstDiv" data-ride="carousel">
	<div class="bestofweek-switchblock">
											
			<p class="best-for-week-par">Лучшее за неделю</p>
			
			<div class="best-for-week-arr">	
				<a class="more-news-switchrs carousel-control left" href="#myCarousel4" role="button" data-slide="prev">
					<img src="<?php bloginfo('template_url'); ?>/img/right-arrow.png">
				</a>

				<a class=" more-news-switchrs carousel-control right" href="#myCarousel4" role="button" data-slide="next">
					<img src="<?php bloginfo('template_url'); ?>/img/left-arrow.png">
				</a>
			</div>
	</div>	 
					
	<div class="carousel-inner " role="listbox">

						 
		

<?php
	$args = array(
			'posts_per_page'	=> 16,
			'post_type'			=> array('post', 'places'),
			'cat'				=> array(get_queried_object_id() * -1),
			'offset'			=> 50			 );
	


		$gvPost = new WP_Query($args);
		
		
		if ( $gvPost->have_posts()  ) {

			for($i = 0; $i < 4; $i++){

				?> <div class="item <?php if (!$i) echo 'active';?>">
				<?php
				for($j = 0; $j < 4; $j++){
						$gvPost->the_post();
						
						
						?>
							
								
							<figure class="moreNewsblocks">
								<a href="<?php get_post_permalink()?>">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive categoryImagesNews slider2-img">
									<figcaption><?php the_title(); ?> </figcaption>
								</a>
							</figure>
								
					
						 <?php
						
				}
				?>
				</div>

				<?php
			}
		}

?>

		
		</div>

				
</div>