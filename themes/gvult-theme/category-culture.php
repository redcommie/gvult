<?php 
	get_header();
?>

<div class="container-fluid category-desc-fluid">

<div class="row">
	<?php
	include 'php/slider-main.php';
	?>
</div>

<?php
	
	$args = array
	(
		'posts_per_page'	=> 3,
		'offset'			=> 6,
		'cat'				=> -611,
		'post_type'			=> array('post', 'places')
	);

	$query = new WP_Query($args);
?>
	<div class="row catdest-row">
		<div class="col-sm-8 col-xs-12 cat-margintop cat-card-block">
			<?php 
			$count = 0;
			while ( $query->have_posts() )
			{
				$query->the_post();
										
				switch ( $count )
				{
					case 0:
						echo '<div class="col-sm-12 col-xs-12">';
						echo getCardSimpleDouble($post);
						echo '</div>';
						break;
					case 1:
					case 2:
						echo '<div class="col-sm-6 col-xs-12 blackoutPictureLink typeBlock_300x335 cat-margintop">';
						echo getCardSimpleSingle($post);
						echo '</div>';
						break;
					default:
						print_var('Что не так. Первый цикл новостей. Файл category.php.');
						break;
				}
				
				$count++;
			}

			wp_reset_postdata(); ?>
		</div>
		<div class="col-xs-12 col-sm-4 catdest-newsblock">
			<div class="col-sm-12 col-xs-12 padding_zero">
				<?php
					include 'php/slider-news.php';
				?>
			</div>
		</div>
	
		<div class="col-md-12 catdest-socialrow">
			<div class="col-sm-4 restCard1">
				<a href="#">
						<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
				</a>
			</div>
			<div class="col-sm-4 restCard1">
				<a href="#">
						<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
				</a>
			</div>
			<div class="col-sm-4 catdest-socialblock">
				<div class="fb-page" 
				  data-href="https://www.facebook.com/GVULT/"
				  data-width="282" 
				  data-hide-cover="false"
				  data-show-facepile="true" 
				  data-show-posts="false">
				</div>
			</div>
		</div>
		
		
	</div>
	<div class="row margin_zero">
		<?php
		$args = array
		(
			'posts_per_page'	=> 5,
			'offset'			=> 9,
			'cat'				=> -611,
			'post_type'			=> array('post', 'places')
		);

		$query = new WP_Query($args);

		$count = 0;
		while ( $query->have_posts() ){
			$query->the_post();
									
			switch ( $count )
			{
				case 0:
				case 4:
					echo '<div class="col-sm-8 cat-margintop">';
					echo getCardSimpleDouble($post);;
					echo '</div>';
					break;
				case 1:
					echo '<div class="col-sm-4 blackoutPictureLink typeBlock_300x335 cat-margintop">';
					echo getCardSimpleSingle($post);;
					echo '</div>';
					break;
				case 2:
					echo '<div class="col-sm-12 blackoutPictureLink cat-margintop catdest_card_delete">';
					echo getCardSimpleTriple($post);;
					echo '</div>';
					break;
				case 3:
					echo '<div class="col-sm-4 blackoutPictureLink typeBlock_300x335 cat-margintop">';
					echo getCardSimpleSingle($post);;
					echo '</div>';
					break;	
				default:
					print_var('Что не так. Второй цикл новостей. Файл category.php.');
					break;
			}
			
			$count++;
		}

		wp_reset_postdata();?>
	</div>
	<div class="row catdest-row catdest-phone-lastblock">
			<div class="col-sm-8 cat-margintop lastblock_card_phone">
				<?php
				$args = array
				(
					'posts_per_page'	=> 3,
					'offset'			=> 14,
					'cat'				=> -611,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);

				$count = 0;
				while ( $query->have_posts() ){
					$query->the_post();
											
					switch ( $count )
					{
						case 0:
						case 1:
							echo '<div class="col-sm-6 blackoutPictureLink typeBlock_300x335 cat-margintop">';
							echo getCardSimpleSingle($post);
							echo '</div>';
							break;
						case 2:
							echo '<div class="col-sm-12 catdest_card_delete">';
							echo getCardSimpleDouble($post);
							echo '</div>';
							break;
						default:
							print_var('Что не так. Третий цикл новостей. Файл category.php.');
							break;
					}
					
					$count++;
				}

				wp_reset_postdata();?>
			</div>
			<div class="col-sm-4 catdest-newsblock">
				<div class="col-sm-12 catdest-newsblock1">
					<?php
						include 'php/slider-bestofweek.php';
					?>
				</div>
			</div>
			<div id="wr_btnMorePosts" class="col-sm-12 item-lastBtnBlk category-lastBtn">
				<?php include 'php/btn_load_more_posts.php';?>
			</div>
		</div>
</div>

<?php 
	get_footer(); 
?>